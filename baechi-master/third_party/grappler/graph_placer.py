# Copyright 2018 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Graph Placer."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import time
from tensorflow.python.platform import tf_logging
from tensorflow.core.protobuf import meta_graph_pb2
from tensorflow.core.protobuf import rewriter_config_pb2
from tensorflow.python.framework import errors
from tensorflow.python.framework import ops as tf_ops
from tensorflow.python.grappler import cluster as gcluster
from tensorflow.python.grappler import item as gitem
from tensorflow.python.grappler import tf_optimizer
from tensorflow.python.training import training

from . import hierarchical_controller


def PlaceGraph(metagraph,
               cluster=None,
               allotted_time=3600,
               hparams=None,
               verbose=False,
               sess_config=None,
               gpu_only=False):
  """Place the provided metagraph.

  Args:
    metagraph: the metagraph to place.
    cluster: an optional set of hardware resource to optimize the placement for.
      If none is specified, we'll optimize the placement for the hardware
      available on the local machine.
    allotted_time: the maximum amount to time in seconds to spend optimizing
      the placement.
    hparams: hyperparameters used to fine tune the placer.
    verbose: prints debug information if True.
    sess_config: ConfigProto proto for the session.
    gpu_only: use only gpu devices in the cluster.

  Returns:
    A list of placed metagraphs.
  """
  metagraph_list = []
  if cluster is None:
    cluster = gcluster.Cluster()

  metagraph_copy = meta_graph_pb2.MetaGraphDef()
  metagraph_copy.CopyFrom(metagraph)
  item = gitem.Item(metagraph_copy, ignore_colocation=False)
  # item = gitem.Item(metagraph_copy, ignore_colocation=True)


  # Measure the runtime achievable with the original placement.
  try:
    # 这里初始化测试是默认用的单卡，这样当batch size加大时会OOM
    # _, original_run_time, _ = cluster.MeasureCosts(item)
    original_run_time = 100
    metagraph_list.append(metagraph)
    if verbose:
      tf_logging.info("Runtime for original placement: " +
                      str(original_run_time))
  except errors.OpError as e:
    if verbose:
      tf_logging.info("Original placement isn't feasible: " + str(e))
    original_run_time = hparams.failing_signal

  if hparams is None:
    hparams = hierarchical_controller.hierarchical_controller_hparams()
  # We run with a single child
  hparams.num_children = 1

  with tf_ops.Graph().as_default():
    # Place all the nodes of the controller on the CPU. We don't want them to
    # fight for accelerator memory with the model to optimize.
    with tf_ops.device("/device:CPU:0"):
      model = hierarchical_controller.HierarchicalController(
          hparams, item, cluster, gpu_only=gpu_only)
      ops = model.build_controller()
      session_creator = training.ChiefSessionCreator(config=sess_config)
      with training.MonitoredSession(session_creator=session_creator) as sess:
        start_time = time.time()
        current_time = start_time

        step = 0
        same_times = 0
        ops_dict = {}
        best_run_time = original_run_time
        find_best_spend_time = 0
        while current_time - start_time < allotted_time:
          step += 1

          grouping_actions = model.generate_grouping(sess)
          input_to_seq2seq = model.create_group_embeddings(
              grouping_actions, verbose=verbose)
          model.generate_placement(input_to_seq2seq, sess)
          try:
            run_time = model.eval_placement(
                sess,
                verbose=verbose)
          except errors.OpError as e:
            if verbose:
              tf_logging.info("Failed to run graph:" + str(e))
            run_time = hparams.failing_signal
          updated = model.update_reward(sess, run_time, verbose=verbose)
          # if updated and run_time < original_run_time:
          if updated and run_time < best_run_time:
            if verbose:
              tf_logging.info(
                  "Found better placement, with runtime " + str(run_time))
            new_metagraph = meta_graph_pb2.MetaGraphDef()
            new_metagraph.CopyFrom(metagraph)
            model.export_placement(new_metagraph)
            metagraph_list.append(new_metagraph)

            tf_logging.info("----------------")
            t1 = current_time - start_time
            tf_logging.info("step: " + str(step) + " time: " + str(t1) + " / " + str(allotted_time))
            # 打印放置信息
            ops_dict = {}
            for node in new_metagraph.graph_def.node:
              if ops_dict.__contains__(node.device):
                ops_dict[node.device] += 1
              else:
                ops_dict[node.device] = 1
            find_best_spend_time = current_time - start_time
            best_run_time = run_time
          else:
            t1 = current_time - start_time
            tf_logging.info("step: " + str(step) + " time: " + str(t1) + " / " + str(allotted_time))
          # else:
          if len(ops_dict) > 0:
            tf_logging.info("----place information------------")
            tf_logging.info("now step: " + str(step))
            for k, v in ops_dict.items():
              tf_logging.info("device: %s num: %d", k, v)
            tf_logging.info("best run time: " + str(best_run_time) + " search time: " + str(find_best_spend_time))

          model.process_reward(sess)

          current_time = time.time()


          # if same_times == 100:
          #   break

  return metagraph_list
