import networkx as nx
import matplotlib.pyplot as plt
from genetic_search.genetic_lib.genetic_place import Genetic_placer


def create_colocation_group_to_ops_map(op_graph):
    """Generate a dict that maps a colocation group to its op id list."""
    retval = {}

    for op_id, op_data in op_graph.nodes().items():
        # assume there is only one group
        group = op_data['colocation_group'][0]
        if group in retval:
            retval[group].append(op_id)
        else:
            retval[group] = [op_id]

    return retval

def create_colocation_group_infos(op_graph):
    """Generate a dict that maps a colocation group to its information.

    dict value is another dict that has following entries.
        "ops": a list of op data that are in the colocation group.
        "temp_memory_max": max temp memory of ops in the group.
        "output_memory_max": max output memory of ops in the group.
        "persistent_memory_sum": accumulated persistent memory of ops
                                 in the colocation group.
    """
    colocation_group_infos = {
        group_name: {"ops": [op_graph.nodes[op_id] for op_id in op_ids]}
        for group_name, op_ids
        in create_colocation_group_to_ops_map(op_graph).items()}

    # calculate memory requirement for each group
    for _, group_info in colocation_group_infos.items():
        temp_memory_max = 0
        output_memory_max = 0
        persistent_memory_sum = 0
        output_memory_sum = 0

        for op in group_info["ops"]:
            temp_memory_max = max(temp_memory_max, op["temporary_memory"])
            current_out_memory_sum = sum(op["output_memory"])
            output_memory_max = max(output_memory_max, current_out_memory_sum)
            persistent_memory_sum += op["persistent_memory"]
            output_memory_sum += current_out_memory_sum

        group_info["temp_memory_max"] = temp_memory_max
        group_info["output_memory_max"] = output_memory_max
        group_info["persistent_memory_sum"] = persistent_memory_sum
        group_info["output_memory_sum"] = output_memory_sum

    return colocation_group_infos

op_graph = nx.read_gpickle("graph_gpickles/op_graph.gpickle")

topo_graph = nx.read_gpickle("graph_gpickles/topo_graph.gpickle")
sct_graph = nx.read_gpickle("graph_gpickles/sct_graph.gpickle")
etf_graph = nx.read_gpickle("graph_gpickles/etf_graph.gpickle")


device_graph = nx.read_gpickle("graph_gpickles/device_graph.gpickle")


colocation_group = create_colocation_group_infos(topo_graph)
geneticPlacer = Genetic_placer(op_graph=op_graph, device_graph=device_graph, colocation_group=colocation_group,
                               topo=topo_graph,sct=sct_graph,etf=etf_graph)
geneticPlacer.evolutionOfCommunity()
print()