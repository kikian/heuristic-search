import copy
import random
from utils import logger
from placer import placer_utils
import time
from placer.virtual_scheduler import VirtualScheduler


_LOGGER = logger.get_logger(__file__, level=logger.INFO)

class Genetic_placer:

    # 为群落随机分配设备
    def initial_graph_placement(self, op_graph):
        device_num = len(self.device_graph.nodes())
        ops_count = 0
        op_graph_ops_dict = op_graph.nodes()._nodes
        for id, data in op_graph_ops_dict.items():
            target_device = random.randint(0, device_num-1)
            # 如果没有放置过
            if 'p' not in data:
                # 随机指定一个目标设备
                data['p'] = target_device
                # _LOGGER.info("[placement] %s: %d", data["name"], data["p"])
                # 将同组的设备一起调度
                co_group = data['colocation_group'][0]
                ops_count = ops_count + 1
                if len(self.colocation_group[co_group]['ops']) > 1:
                    for group_member_ops in self.colocation_group[co_group]['ops']:
                        ops_id = group_member_ops['id']
                        if 'p' not in op_graph_ops_dict[ops_id]:
                            op_graph_ops_dict[ops_id]['p'] = target_device
                            # _LOGGER.info("[placement] %s: %d", op_graph_ops_dict[ops_id]["name"],op_graph_ops_dict[ops_id]["p"])
                            ops_count = ops_count + 1

        _LOGGER.info("initial finished total: %d / %d", ops_count, len(op_graph_ops_dict))



    def list_of_groups(self, list_info, per_list_len):
        '''
        :param list_info:   列表
        :param per_list_len:  每个小列表的长度
        :return:
        '''
        list_of_group = zip(*(iter(list_info),) * per_list_len)
        end_list = [list(i) for i in list_of_group]  # i is a tuple
        count = len(list_info) % per_list_len
        end_list.append(list_info[-count:]) if count != 0 else end_list
        return end_list

    def genesVariation(self, variation_group):
        assert len(variation_group) % 2 == 0
        # 对该组内的群落进行基因变异
        new_variation_group = []
        for index in variation_group:
            new_community = copy.deepcopy(self.graph_communities[index])
            self.random_change_graph_placement(new_community)
            new_variation_group.append(new_community)
        return new_variation_group

    def genesSwap(self, swap_group):
        assert len(swap_group) % 2 == 0
        # 对当前群落进行基因互换
        group_start_index = 0
        new_swap_group = []
        while group_start_index < len(swap_group):
            community1_index = swap_group[group_start_index]
            community2_index = swap_group[group_start_index + 1]
            new_community1 = copy.deepcopy(self.graph_communities[community1_index])
            new_community2 = copy.deepcopy(self.graph_communities[community2_index])

            new_community1_op_graph_ops_dict = new_community1.nodes()._nodes
            new_community2_op_graph_ops_dict = new_community2.nodes()._nodes

            # 变化三个基因
            changed_flag = False
            for i in range(3):
                # 随机选择一个算子
                target_op = random.randint(0, self.graph_nodes_num - 1)
                device1 = new_community1_op_graph_ops_dict[target_op]['p']
                device2 = new_community2_op_graph_ops_dict[target_op]['p']
                if device1 != device2:
                    # 互换基因
                    self.change_graph_placement(new_community1_op_graph_ops_dict, target_op, device2)
                    self.change_graph_placement(new_community2_op_graph_ops_dict, target_op, device1)
                    changed_flag = True
            # 如果群落发生了变化
            if changed_flag:
                new_swap_group.append(new_community1)
                new_swap_group.append(new_community2)

            group_start_index += 2
        return new_swap_group

    # 群落进化迭代
    def evolutionOfCommunity(self):

        evolution_start = time.time()
        iterations = 0
        # index_lst 作为下标，方便高效打乱
        index_lst = []
        for i in range(len(self.graph_communities)):
            index_lst.append(i)

        while True:
            iter_start = time.time()
            # 随机打乱下标
            random.shuffle(index_lst)
            groups = self.list_of_groups(index_lst, int(len(self.graph_communities) / 2))
            # 基因变异
            new_variation_group = self.genesVariation(groups[0])
            # 基因互换
            new_swap_group = self.genesSwap(groups[1])

            print(len(new_variation_group))
            print(len(new_swap_group))

            self.graph_communities.extend(new_variation_group)
            self.graph_communities.extend(new_swap_group)

            simulate_res_lst = []
            # todo: 优化不重复模拟执行那些没有变化的策略
            for graph_index, graph in enumerate(self.graph_communities):
                device_graph = copy.deepcopy(self.device_graph)
                scheduler = VirtualScheduler(graph, device_graph)
                scheduler.initialize()
                makespan = scheduler.run()
                simulate_res_lst.append([graph_index, makespan])

            simulate_res_lst.sort(key=lambda simulate_res: simulate_res[1])

            new_graph_communities = []
            for simulate_res in simulate_res_lst:
                if len(new_graph_communities) < self.community_size:
                    new_graph_communities.append(self.graph_communities[simulate_res[0]])
                _LOGGER.info("index %d makespan: %d", simulate_res[0], simulate_res[1])

            _LOGGER.info("new graph communities len: %d", len(new_graph_communities))
            self.graph_communities = new_graph_communities
            iter_end = time.time()
            iterations = iterations + 1
            _LOGGER.info(" iterations: %d spend time %d ms",
                         iterations, iter_end - iter_start)
            if iterations == 30:
                break
        evolution_end = time.time()

        _LOGGER.info("evolution total spend %d ms, The number of iterations: %d",
                     evolution_end - evolution_start, iterations)



    # 为群落随机分配设备
    def change_graph_placement(self, community_op_graph_ops_dict, target_op, target_device):
        # 随机指定一个目标设备
        community_op_graph_ops_dict[target_op]['p'] = target_device
        # 将同组的设备一起调度
        co_group = community_op_graph_ops_dict[target_op]['colocation_group'][0]
        if len(self.colocation_group[co_group]['ops']) > 1:
            for group_member_ops in self.colocation_group[co_group]['ops']:
                ops_id = group_member_ops['id']
                community_op_graph_ops_dict[ops_id]['p'] = target_device

    # 为群落随机分配设备
    def random_change_graph_placement(self, op_graph):
        device_num = len(self.device_graph.nodes())
        op_graph_ops_dict = op_graph.nodes()._nodes

        stop_flag = False
        while not stop_flag:
            # 随机选择一个算子
            target_op = random.randint(0, len(op_graph_ops_dict) - 1)
            # 随机选择一个设备
            target_device = random.randint(0, device_num - 1)
            # 如果当前选择的设备和之前一样则重新
            if op_graph_ops_dict[target_op]['p'] == target_device:
                continue
            else:
                stop_flag = True

            # 指定一个目标设备
            self.change_graph_placement(op_graph_ops_dict, target_op, target_device)

    # 初始化群落
    def init_graph_community(self, op_graph):

        topo_graph = self.topo(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))
        sct_graph = self.sct(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))
        etf_graph = self.etf(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))

        # 三种启发式算法的策略图
        self.graph_communities.append(topo_graph)
        self.graph_communities.append(sct_graph)
        self.graph_communities.append(etf_graph)

        # 在这三种策略图的基础上随机变异 生成新的群落
        index = 3
        while len(self.graph_communities) < self.community_size:
            if index % 3 == 0:
                # topo_graph
                new_community = copy.deepcopy(topo_graph)
                self.random_change_graph_placement(new_community)
                self.graph_communities.append(new_community)
            elif index % 3 == 1:
                # sct_graph
                new_community = copy.deepcopy(sct_graph)
                self.random_change_graph_placement(new_community)
                self.graph_communities.append(new_community)
            else:
                # etf_graph
                new_community = copy.deepcopy(etf_graph)
                self.random_change_graph_placement(new_community)
                self.graph_communities.append(new_community)
            index = index + 1

        _LOGGER.info("init total %d graph community finished", len(self.graph_communities))



    def __init__(self, op_graph, device_graph, initial_fun):
        # 进化群落的大小设为15
        self.community_size = 16
        self.graph_communities = []
        self.graph_nodes_num = len(op_graph.nodes()._nodes)
        # 初始化的搜索方法
        self.topo = initial_fun[0]
        self.sct = initial_fun[1]
        self.etf = initial_fun[2]

        self.colocation_group  = placer_utils.create_colocation_group_infos(op_graph)

        self.device_graph = device_graph
        # 初始化
        self.init_graph_community(op_graph=op_graph)





