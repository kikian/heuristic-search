import copy
import random
from utils import logger
from placer import placer_utils

_LOGGER = logger.get_logger(__file__, level=logger.INFO)

class Genetic_placer:

    # 为群落随机分配设备
    def initial_graph_placement(self, op_graph):
        device_num = len(self.device_graph.nodes())
        ops_count = 0
        op_graph_ops_dict = op_graph.nodes()._nodes
        for id, data in op_graph_ops_dict.items():
            target_device = random.randint(0, device_num-1)
            # 如果没有放置过
            if 'p' not in data:
                # 随机指定一个目标设备
                data['p'] = target_device
                # _LOGGER.info("[placement] %s: %d", data["name"], data["p"])
                # 将同组的设备一起调度
                co_group = data['colocation_group'][0]
                ops_count = ops_count + 1
                if len(self.colocation_group[co_group]['ops']) > 1:
                    for group_member_ops in self.colocation_group[co_group]['ops']:
                        ops_id = group_member_ops['id']
                        if 'p' not in op_graph_ops_dict[ops_id]:
                            op_graph_ops_dict[ops_id]['p'] = target_device
                            # _LOGGER.info("[placement] %s: %d", op_graph_ops_dict[ops_id]["name"],op_graph_ops_dict[ops_id]["p"])
                            ops_count = ops_count + 1

        _LOGGER.info("initial finished total: %d / %d", ops_count, len(op_graph_ops_dict))


    # 为群落随机分配设备
    def random_change_graph_placement(self, op_graph):
        device_num = len(self.device_graph.nodes())
        op_graph_ops_dict = op_graph.nodes()._nodes

        ops_count = len(op_graph_ops_dict)

        for i in range(3):
            target_op = random.randint(0, ops_count - 1)
            try:
                target_device = random.randint(0, device_num - 1)
                # 随机指定一个目标设备
                op_graph_ops_dict[target_op]['p'] = target_device
            except:
                print(target_op)
                print(op_graph_ops_dict[target_op])
                exit(1)

            # 将同组的设备一起调度
            co_group = op_graph_ops_dict[target_op]['colocation_group'][0]
            ops_count = ops_count + 1
            if len(self.colocation_group[co_group]['ops']) > 1:
                for group_member_ops in self.colocation_group[co_group]['ops']:
                    ops_id = group_member_ops['id']

                    try:
                        op_graph_ops_dict[ops_id]['p'] = target_device
                    except:
                        print(target_device)
                        print(op_graph_ops_dict[ops_id])
                        exit(1)
                    ops_count = ops_count + 1

    # 初始化群落
    def init_graph_community(self, op_graph):

        topo_graph = self.topo(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))
        sct_graph = self.sct(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))
        etf_graph = self.etf(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))

        self.graph_communities.append(topo_graph)
        self.graph_communities.append(sct_graph)
        self.graph_communities.append(etf_graph)

        # for i in range(self.community_size):
        #     _LOGGER.info("placing %d", i)
        #     new_community = copy.deepcopy(op_graph)
        #     self.initial_graph_placement(op_graph=new_community)
        #     self.graph_communities.append(new_community)

        while len(self.graph_communities) < self.community_size:
            new_community = copy.deepcopy(topo_graph)
            self.random_change_graph_placement(new_community)
            self.graph_communities.append(new_community)

            new_community = copy.deepcopy(sct_graph)
            self.random_change_graph_placement(new_community)
            self.graph_communities.append(new_community)

            new_community = copy.deepcopy(etf_graph)
            self.random_change_graph_placement(new_community)
            self.graph_communities.append(new_community)

        _LOGGER.info("init total %d graph community finished", len(self.graph_communities))



    def __init__(self, op_graph, device_graph, initial_fun):
        # 进化群落的大小
        self.community_size = 30
        self.graph_communities = []
        # 初始化的搜索方法
        self.topo = initial_fun[0]
        self.sct = initial_fun[1]
        self.etf = initial_fun[2]

        self.colocation_group  = placer_utils.create_colocation_group_infos(op_graph)

        self.device_graph = device_graph
        self.init_graph_community(op_graph=op_graph)



