
docker pull tensorflow/tensorflow:1.12.0-devel-gpu-py3

  

https://pypi.org/project/cvxopt/1.2.6/#files

python train.py --costgen --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=128 --model_name=gnmt_v2 --vocab_size=30000 --max_seq_length=40 --rnn_unit_type=lstm --rnn_units=512 --num_layers=4 --encoder_type=gnmt --num_gpus=2 --residual --colocate_grads_with_ops --only_forward

```shell


# 寻找策略
# nlp
# gnmt_v2 2卡
# profile
`
CUDA_VISIBLE_DEVICES="0,1" python train.py --costgen --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=128 --model_name=gnmt_v2 --vocab_size=30000 --max_seq_length=40 --rnn_unit_type=lstm --rnn_units=512 --num_layers=4 --encoder_type=gnmt --num_gpus=2 --residual --colocate_grads_with_ops --only_forward
`
# search

CUDA_VISIBLE_DEVICES="0,1" python train.py --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=128 --model_name=gnmt_v2 --vocab_size=30000 --max_seq_length=40 --rnn_unit_type=lstm --rnn_units=512 --num_layers=4 --encoder_type=gnmt --num_gpus=2 --residual --colocate_grads_with_ops --only_forward --placement_method=m_topo --placer_type=fusion --grouper=coplace --comm_cost_coeffs=0.0001027,271 --memory_fraction=1.0
CUDA_VISIBLE_DEVICES="0,1" python train.py --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=128 --model_name=gnmt_v2 --vocab_size=30000 --max_seq_length=40 --rnn_unit_type=lstm --rnn_units=512 --num_layers=4 --encoder_type=gnmt --num_gpus=2 --residual --colocate_grads_with_ops --only_forward --placement_method=m_sct --placer_type=fusion --grouper=coplace --comm_cost_coeffs=0.0001027,271 --memory_fraction=1.0
CUDA_VISIBLE_DEVICES="0,1" python train.py --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=128 --model_name=gnmt_v2 --vocab_size=30000 --max_seq_length=40 --rnn_unit_type=lstm --rnn_units=512 --num_layers=4 --encoder_type=gnmt --num_gpus=2 --residual --colocate_grads_with_ops --only_forward --placement_method=m_etf --placer_type=fusion --grouper=coplace --comm_cost_coeffs=0.0001027,271 --memory_fraction=1.0
`
# gnmt_v2 4卡
# profile
CUDA_VISIBLE_DEVICES="0,1,2,3" python train.py --costgen --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=128 --model_name=gnmt_v2 --vocab_size=30000 --max_seq_length=40 --rnn_unit_type=lstm --rnn_units=512 --num_layers=4 --encoder_type=gnmt --num_gpus=4 --residual --colocate_grads_with_ops --only_forward
# search
CUDA_VISIBLE_DEVICES="0,1,2,3" python train.py --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=128 --model_name=gnmt_v2 --vocab_size=30000 --max_seq_length=40 --rnn_unit_type=lstm --rnn_units=512 --num_layers=4 --encoder_type=gnmt --num_gpus=4 --residual --colocate_grads_with_ops --only_forward --placement_method=m_topo --placer_type=fusion --grouper=coplace --comm_cost_coeffs=0.0001027,271 --memory_fraction=1.0
CUDA_VISIBLE_DEVICES="0,1,2,3" python train.py --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=128 --model_name=gnmt_v2 --vocab_size=30000 --max_seq_length=40 --rnn_unit_type=lstm --rnn_units=512 --num_layers=4 --encoder_type=gnmt --num_gpus=4 --residual --colocate_grads_with_ops --only_forward --placement_method=m_sct --placer_type=fusion --grouper=coplace --comm_cost_coeffs=0.0001027,271 --memory_fraction=1.0
CUDA_VISIBLE_DEVICES="0,1,2,3" python train.py --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=128 --model_name=gnmt_v2 --vocab_size=30000 --max_seq_length=40 --rnn_unit_type=lstm --rnn_units=512 --num_layers=4 --encoder_type=gnmt --num_gpus=4 --residual --colocate_grads_with_ops --only_forward --placement_method=m_etf --placer_type=fusion --grouper=coplace --comm_cost_coeffs=0.0001027,271 --memory_fraction=1.0

# gnmt_v2 6卡
# profile
CUDA_VISIBLE_DEVICES="0,1,2,3,4,5" python train.py --costgen --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=128 --model_name=gnmt_v2 --vocab_size=30000 --max_seq_length=40 --rnn_unit_type=lstm --rnn_units=512 --num_layers=4 --encoder_type=gnmt --num_gpus=6 --residual --colocate_grads_with_ops --only_forward
# search
CUDA_VISIBLE_DEVICES="0,1,2,3,4,5" python train.py --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=128 --model_name=gnmt_v2 --vocab_size=30000 --max_seq_length=40 --rnn_unit_type=lstm --rnn_units=512 --num_layers=4 --encoder_type=gnmt --num_gpus=6 --residual --colocate_grads_with_ops --only_forward --placement_method=m_topo --placer_type=fusion --grouper=coplace --comm_cost_coeffs=0.0001027,271 --memory_fraction=1.0
CUDA_VISIBLE_DEVICES="0,1,2,3,4,5" python train.py --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=128 --model_name=gnmt_v2 --vocab_size=30000 --max_seq_length=40 --rnn_unit_type=lstm --rnn_units=512 --num_layers=4 --encoder_type=gnmt --num_gpus=6 --residual --colocate_grads_with_ops --only_forward --placement_method=m_sct --placer_type=fusion --grouper=coplace --comm_cost_coeffs=0.0001027,271 --memory_fraction=1.0
CUDA_VISIBLE_DEVICES="0,1,2,3,4,5" python train.py --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=128 --model_name=gnmt_v2 --vocab_size=30000 --max_seq_length=40 --rnn_unit_type=lstm --rnn_units=512 --num_layers=4 --encoder_type=gnmt --num_gpus=6 --residual --colocate_grads_with_ops --only_forward --placement_method=m_etf --placer_type=fusion --grouper=coplace --comm_cost_coeffs=0.0001027,271 --memory_fraction=1.0


# cv
# inception_v3 2卡
# profile
CUDA_VISIBLE_DEVICES="0,1" python train.py --costgen --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=2 --model_name=inception_v3 --num_gpus=2 --residual --colocate_grads_with_ops --only_forward
# search
CUDA_VISIBLE_DEVICES="0,1" python train.py --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=2 --model_name=inception_v3 --num_gpus=2 --residual --colocate_grads_with_ops --only_forward --placement_method=m_topo --placer_type=fusion --grouper=coplace --comm_cost_coeffs=0.0001027,271 --memory_fraction=1.0
CUDA_VISIBLE_DEVICES="0,1" python train.py --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=2 --model_name=inception_v3 --num_gpus=2 --residual --colocate_grads_with_ops --only_forward --placement_method=m_sct --placer_type=fusion --grouper=coplace --comm_cost_coeffs=0.0001027,271 --memory_fraction=1.0
CUDA_VISIBLE_DEVICES="0,1" python train.py --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=2 --model_name=inception_v3 --num_gpus=2 --residual --colocate_grads_with_ops --only_forward --placement_method=m_etf --placer_type=fusion --grouper=coplace --comm_cost_coeffs=0.0001027,271 --memory_fraction=1.0

# inception_v3 4卡
# profile
CUDA_VISIBLE_DEVICES="0,1,2,3" python train.py --costgen --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=2 --model_name=inception_v3 --num_gpus=4 --residual --colocate_grads_with_ops --only_forward
# search
CUDA_VISIBLE_DEVICES="0,1,2,3" python train.py --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=2 --model_name=inception_v3 --num_gpus=4 --residual --colocate_grads_with_ops --only_forward --placement_method=m_topo --placer_type=fusion --grouper=coplace --comm_cost_coeffs=0.0001027,271 --memory_fraction=1.0
CUDA_VISIBLE_DEVICES="0,1,2,3" python train.py --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=2 --model_name=inception_v3 --num_gpus=4 --residual --colocate_grads_with_ops --only_forward --placement_method=m_sct --placer_type=fusion --grouper=coplace --comm_cost_coeffs=0.0001027,271 --memory_fraction=1.0
CUDA_VISIBLE_DEVICES="0,1,2,3" python train.py --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=2 --model_name=inception_v3 --num_gpus=4 --residual --colocate_grads_with_ops --only_forward --placement_method=m_etf --placer_type=fusion --grouper=coplace --comm_cost_coeffs=0.0001027,271 --memory_fraction=1.0

# inception_v3 6卡
# profile
CUDA_VISIBLE_DEVICES="0,1,2,3,4,5" python train.py --costgen --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=2 --model_name=inception_v3 --num_gpus=6 --residual --colocate_grads_with_ops --only_forward
# search
CUDA_VISIBLE_DEVICES="0,1,2,3,4,5" python train.py --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=2 --model_name=inception_v3 --num_gpus=6 --residual --colocate_grads_with_ops --only_forward --placement_method=m_topo --placer_type=fusion --grouper=coplace --comm_cost_coeffs=0.0001027,271 --memory_fraction=1.0
CUDA_VISIBLE_DEVICES="0,1,2,3,4,5" python train.py --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=2 --model_name=inception_v3 --num_gpus=6 --residual --colocate_grads_with_ops --only_forward --placement_method=m_sct --placer_type=fusion --grouper=coplace --comm_cost_coeffs=0.0001027,271 --memory_fraction=1.0
CUDA_VISIBLE_DEVICES="0,1,2,3,4,5" python train.py --cost_path=/tmp/cost.pkl --optimizer=adam --batch_size=2 --model_name=inception_v3 --num_gpus=6 --residual --colocate_grads_with_ops --only_forward --placement_method=m_etf --placer_type=fusion --grouper=coplace --comm_cost_coeffs=0.0001027,271 --memory_fraction=1.0

```