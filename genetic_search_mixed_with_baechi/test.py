import networkx as nx
import matplotlib.pyplot as plt
# from genetic_search.genetic_lib.genetic_place import Genetic_placer
import random
a = range(4)
x, y = random.sample(a, 2)
print(123)

d = [2, 6, 3, 7, 9, 5]
index = d.index(min(d))

def create_colocation_group_to_ops_map(op_graph):
    """Generate a dict that maps a colocation group to its op id list."""
    retval = {}

    for op_id, op_data in op_graph.nodes().items():
        # assume there is only one group
        group = op_data['colocation_group'][0]
        if group in retval:
            retval[group].append(op_id)
        else:
            retval[group] = [op_id]

    return retval

def create_colocation_group_infos(op_graph):
    """Generate a dict that maps a colocation group to its information.

    dict value is another dict that has following entries.
        "ops": a list of op data that are in the colocation group.
        "temp_memory_max": max temp memory of ops in the group.
        "output_memory_max": max output memory of ops in the group.
        "persistent_memory_sum": accumulated persistent memory of ops
                                 in the colocation group.
    """
    colocation_group_infos = {
        group_name: {"ops": [op_graph.nodes[op_id] for op_id in op_ids]}
        for group_name, op_ids
        in create_colocation_group_to_ops_map(op_graph).items()}

    # calculate memory requirement for each group
    for _, group_info in colocation_group_infos.items():
        temp_memory_max = 0
        output_memory_max = 0
        persistent_memory_sum = 0
        output_memory_sum = 0

        for op in group_info["ops"]:
            temp_memory_max = max(temp_memory_max, op["temporary_memory"])
            current_out_memory_sum = sum(op["output_memory"])
            output_memory_max = max(output_memory_max, current_out_memory_sum)
            persistent_memory_sum += op["persistent_memory"]
            output_memory_sum += current_out_memory_sum

        group_info["temp_memory_max"] = temp_memory_max
        group_info["output_memory_max"] = output_memory_max
        group_info["persistent_memory_sum"] = persistent_memory_sum
        group_info["output_memory_sum"] = output_memory_sum

    return colocation_group_infos

op_graph = nx.read_gpickle("./graph_gpickles/op_graph.gpickle")

topo_graph = nx.read_gpickle("./graph_gpickles/topo_graph.gpickle")
sct_graph = nx.read_gpickle("./graph_gpickles/sct_graph.gpickle")
etf_graph = nx.read_gpickle("./graph_gpickles/etf_graph.gpickle")

device_graph = nx.read_gpickle("./graph_gpickles/device_graph.gpickle")


device_graph_dict = device_graph.nodes()._nodes
device_num = len(device_graph_dict)

def caculateTotalOutputMemory(graph):
    succ_dict = graph.succ._atlas
    total = 0
    op_graph_ops_dict = graph.nodes()._nodes

    for pid, node_dict in succ_dict.items():
        if len(node_dict) > 0:
            for succ_id, node in node_dict.items():
                assert len(node['tensor']) == 1
                # 过滤掉那些在同一个设备上的通信
                if op_graph_ops_dict[succ_id]['p'] != op_graph_ops_dict[pid]['p']:
                    total = total + node['tensor'][0]['num_bytes']

    return total

def caculateTotalOutputMemory2(graph):
    totalOutputMemory = 0
    op_graph_ops_dict = graph.nodes()._nodes

    lst = {}
    for parent_id, parent_node in op_graph_ops_dict.items():
        descend_ids = graph.successors(parent_id)

        if len(parent_node['output_memory']) > 0:
            for succ_id in descend_ids:
                pair = str(parent_id) + "_" + str(succ_id)
                lst[pair] = parent_node['output_memory'][0]
                if op_graph_ops_dict[succ_id]['p'] != parent_node['p']:
                    totalOutputMemory += parent_node['output_memory'][0]
    # sorted(lst.items(), reverse=True)
    lst = sorted(lst.items(), key=lambda item: item[1], reverse=True)

    return totalOutputMemory,lst

import copy
origin = copy.deepcopy(etf_graph)

total_origin, lst = caculateTotalOutputMemory2(origin)

group = {}
has_put = {}
# 根据组进行group



op_graph_ops_dict = origin.nodes()._nodes
for k,v in op_graph_ops_dict.items():
    v['p'] = 0

total_origin = caculateTotalOutputMemory(origin)

total_topo = caculateTotalOutputMemory(topo_graph)
total_sct = caculateTotalOutputMemory(sct_graph)
total_etf = caculateTotalOutputMemory(etf_graph)

total_origin2 = caculateTotalOutputMemory2(origin)
total_topo2 = caculateTotalOutputMemory2(topo_graph)
total_sct2 = caculateTotalOutputMemory2(sct_graph)
total_etf2 = caculateTotalOutputMemory2(etf_graph)


# 计算负载均衡
def caculate_offload(graph):
    op_graph_ops_dict = graph.nodes()._nodes
    device_offload_dict = {}
    device_ops_dict = {}
    for id, ops in op_graph_ops_dict.items():
        if not device_offload_dict.__contains__(ops['p']):
            device_offload_dict[ops['p']] = 0
            device_ops_dict[ops['p']] = []
        device_offload_dict[ops['p']] += ops['persistent_memory']
        device_ops_dict[ops['p']].append(id)
    return device_offload_dict, device_ops_dict

device_offload_dict, device_ops_dict = caculate_offload(topo_graph)
device_offload_dict1, device_ops_dict1 = caculate_offload(sct_graph)
device_offload_dict2, device_ops_dict2 = caculate_offload(etf_graph)
device_offload_dict3, device_ops_dict3 = caculate_offload(origin)


import pickle

from sim.tf_placement_sim.tf_pl_simulator import ImportantOpsSimulator
cost_path = "../cost.pkl"
with open(cost_path, 'rb') as f:
    cost_data = pickle.load(f)

device_names = ['/device:GPU:%d' % i for i in range(2)]

mg = cost_data['graphdef']
op_perf = cost_data['op_perf']
step_stats = cost_data['step_stats']

sim = ImportantOpsSimulator(mg, op_perf, step_stats, device_names)

# ungrouped_pl = {}
# for op in mg.node:
#     try:
#         print(device_names.index(op.device))
#         print(op.name)
#         ungrouped_pl[op.name] = device_names.index(op.device)
#     except:
#         ungrouped_pl[op.name] = 0
#         continue
ungrouped_pl = {}
op_graph_ops_dict = etf_graph.nodes()._nodes

# for id, data in op_graph_ops_dict.items():
#     try:
#         ungrouped_pl[data['name']] = device_names.index(data['p'])
#     except:
#         ungrouped_pl[data['name']] = 0
#         continue

d = {}
for op in mg.node:
    ungrouped_pl[op.name] = 0

for id, data in op_graph_ops_dict.items():
    ungrouped_pl[data['name']] = data['p']


run_time, start_times = sim.simulate(ungrouped_pl, sim_mem_usage=True)
print(run_time / 1e6, start_times)

# sim = ImportantOpsSimulator(topo_graph, device_graph, cost_data)

print(123)

# colocation_group = create_colocation_group_infos(topo_graph)
# geneticPlacer = Genetic_placer(op_graph=op_graph, device_graph=device_graph, colocation_group=colocation_group,
#                                topo=topo_graph,sct=sct_graph,etf=etf_graph)
# geneticPlacer.evolutionOfCommunity()
# print()