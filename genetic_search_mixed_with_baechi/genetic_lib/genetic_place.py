import copy
import random
from utils import logger
from placer import placer_utils
import time
from sim.tf_placement_sim.tf_pl_simulator import ImportantOpsSimulator
from genetic_lib.genetic_graph import GeneticGraph
from placer.virtual_scheduler import VirtualScheduler

_LOGGER = logger.get_logger(__file__, level=logger.INFO)

class Genetic_placer:

    # 为群落随机分配设备
    def initial_graph_placement(self, genetic_graph, init_type):
        assert init_type in ["random_gpu", "single_gpu"]

        op_graph = genetic_graph.graph

        ops_count = 0
        op_graph_ops_dict = op_graph.nodes()._nodes
        for ops_id, data in op_graph_ops_dict.items():
            # 如果没有放置过
            if 'p' not in data:
                # 防止初始化出来的策略就出现了OOM，虽然概率极低
                while True:
                    if init_type == "random_gpu":
                        target_device = random.randint(0, self.device_num - 1)
                    elif init_type == "single_gpu":
                        target_device = 0
                    # 随机指定一个目标设备
                    succ_put = self.change_graph_placement(genetic_graph, ops_id, target_device)
                    if succ_put == True:
                        break
            ops_count = ops_count + 1


        _LOGGER.info("initial finished total: %d / %d", ops_count, len(op_graph_ops_dict))

    def genesVariation(self, community):
        # 当前群落的基因图
        op_graph = community.graph
        op_graph_ops_dict = op_graph.nodes()._nodes

        # 随机选择一个基因对
        target_op_pair = random.randint(0, len(self.ops_transfer_tensor) - 1)
        pair_key = self.ops_transfer_tensor[target_op_pair][0]
        # 只改变没改变过的基因
        ops_pair = pair_key.split('_')
        from_ops = int(ops_pair[0])
        to_ops = int(ops_pair[1])

        # 表示该基因存活
        community.survive_genes[pair_key] = 1
        survive_genes_together = copy.deepcopy(community.survive_genes)
        survive_genes_split = copy.deepcopy(community.survive_genes)

        new_graph_together = copy.deepcopy(op_graph)
        new_graph_split = copy.deepcopy(op_graph)

        genetic_graph_together = GeneticGraph(new_graph_together, self.getGraphId(), survive_genes_together,
                                              community.device_used_memory_lst, self.device_num)
        genetic_graph_split = GeneticGraph(new_graph_split, self.getGraphId(), survive_genes_split,
                                           community.device_used_memory_lst, self.device_num)


        # 1. 将这个基因对放一起（from_ops和to_ops）
        retry_times = 0
        while True:
            # 随机选一个 如果发生OOM则重新 直到找到可以放置的
            target_device = random.randint(0, self.device_num - 1)
            succ_put1 = self.change_graph_placement(genetic_graph_together, from_ops, target_device)
            succ_put2 = self.change_graph_placement(genetic_graph_together, to_ops, target_device)
            if succ_put1 and succ_put2:
                # 如果两个算子放置都没有OOM
                break
            else:
                # 如果其中一个放置导致了OOM，则重新搜索，并计数失败次数
                retry_times += 1
                # 如果重新次数到达了阈值
                if retry_times == 10:
                    # 将该策略标记为OOM策略
                    genetic_graph_together.is_OOM = True
                    break

        # 2. 将这个基因对分开放（from_ops和to_ops）
        retry_times = 0
        while True:
            # 随机选两个
            target_device1, target_device2 = random.sample(self.device_index, 2)
            succ_put1 = self.change_graph_placement(genetic_graph_split, from_ops, target_device1)
            succ_put2 = self.change_graph_placement(genetic_graph_split, to_ops, target_device2)
            if succ_put1 and succ_put2:
                # 如果两个算子放置都没有OOM
                break
            else:
                # 如果其中一个放置导致了OOM，则重新搜索，并计数失败次数
                retry_times += 1
                if retry_times == 10:
                    # 将该策略标记为OOM策略
                    genetic_graph_split.is_OOM = True
                    break

        return genetic_graph_together, genetic_graph_split

    def genesSwap(self, community_left, community_right):
        left_survive_genes = community_left.survive_genes
        right_survive_genes = community_right.survive_genes
        #
        community_left_ops_dict = community_left.graph.nodes()._nodes
        community_right_ops_dict = community_right.graph.nodes()._nodes

        new_left_community = GeneticGraph(copy.deepcopy(community_left.graph), self.getGraphId(), survive_genes=None,
                                          device_used_memory_lst=community_left.device_used_memory_lst,
                                          device_num=self.device_num)
        new_right_community = GeneticGraph(copy.deepcopy(community_right.graph), self.getGraphId(), survive_genes=None,
                                           device_used_memory_lst=community_right.device_used_memory_lst,
                                           device_num=self.device_num)
        # 将community_left的优质基因给new_right_community
        for pair_key, survive_time in left_survive_genes.items():
            ops_pair = pair_key.split('_')
            from_ops = int(ops_pair[0])
            to_ops = int(ops_pair[1])
            # 如果存活次数大于2则是优质基因
            self.change_graph_placement(new_right_community, from_ops,
                                        community_left_ops_dict[from_ops]['p'])
            self.change_graph_placement(new_right_community, to_ops,
                                        community_left_ops_dict[to_ops]['p'])

        # 将community_right_ops_dict的优质基因给new_left_community
        for pair_key, survive_time in right_survive_genes.items():
            ops_pair = pair_key.split('_')
            from_ops = int(ops_pair[0])
            to_ops = int(ops_pair[1])
            self.change_graph_placement(new_left_community, from_ops,
                                        community_right_ops_dict[from_ops]['p'])
            self.change_graph_placement(new_left_community, to_ops,
                                        community_right_ops_dict[to_ops]['p'])

        # 把存活基因对清空
        community_left.survive_genes = {}
        community_right.survive_genes = {}

        return new_left_community, new_right_community

    def getGraphId(self):
        self.graph_id += 1
        return self.graph_id

    def getSurviveCommunity(self, community1, community2, community3=None):
        if community3 is None:
            if community1.run_time <= community2.run_time:
                return community1
            else:
                return community2
        else:
            if community1.run_time <= community2.run_time:
                survive_community = community1
            else:
                survive_community = community2
            if survive_community.run_time <= community3.run_time:
                return survive_community
            return community3

    def evolutionOfCommunity(self):
        evolution_start = time.time()
        max_iterations = 500
        iterations = 0

        self.community_left.run_time = self.caculate_run_time(self.community_left)
        self.community_right.run_time = self.caculate_run_time(self.community_right)

        _LOGGER.info("topo graph , run_time: %d ", self.topo_run_time)
        _LOGGER.info("sct graph , run_time: %d ", self.sct_run_time)
        _LOGGER.info("etf graph , run_time: %d ", self.eft_run_time)
        _LOGGER.info("single , run_time: %d ", self.single_run_time)

        best_left_id = self.community_left.graph_id
        best_right_id = self.community_right.graph_id
        same_time = 0
        max_same_time = 8
        while True:
            _LOGGER.info("----------------------------------")
            iter_start = time.time()
            if (iterations + 1) % 5 == 0:
                # 两个群落交换优质基因
                _LOGGER.info("genesSwap ...... ")
                new_left_community, new_right_community = self.genesSwap(self.community_left, self.community_right)
                # 计算时间
                new_left_community.run_time = self.caculate_run_time(new_left_community)
                new_right_community.run_time = self.caculate_run_time(new_right_community)

                _LOGGER.info("left community  id %d , run_time %d ", self.community_left.graph_id, self.community_left.run_time)
                _LOGGER.info("left swap       id %d , run_time %d ", new_left_community.graph_id, new_left_community.run_time)
                _LOGGER.info("right community id %d , run_time %d ", self.community_right.graph_id, self.community_right.run_time)
                _LOGGER.info("right swap      id %d , run_time %d ", new_right_community.graph_id, new_right_community.run_time)

                # 计算存活的群落
                self.community_left = self.getSurviveCommunity(self.community_left, new_left_community)
                self.community_right = self.getSurviveCommunity(self.community_right, new_right_community)
            else:
                _LOGGER.info("genesVariation ...... ")
                # 两个竞争群落各自变异
                # communityLeft变异
                left_together_variation, left_split_variation = self.genesVariation(self.community_left)
                left_together_variation.run_time = self.caculate_run_time(left_together_variation)
                left_split_variation.run_time = self.caculate_run_time(left_split_variation)

                # communityRight变异
                right_together_variation, right_split_variation = self.genesVariation(self.community_right)
                right_together_variation.run_time = self.caculate_run_time(right_together_variation)
                right_split_variation.run_time = self.caculate_run_time(right_split_variation)

                _LOGGER.info("left community  id %d , run_time %d ", self.community_left.graph_id,
                             self.community_left.run_time)
                _LOGGER.info("left together   id %d , run_time %d ", left_together_variation.graph_id,
                             left_together_variation.run_time)
                _LOGGER.info("left split      id %d , run_time %d ", left_split_variation.graph_id,
                             left_split_variation.run_time)
                _LOGGER.info("right community id %d , run_time %d ", self.community_right.graph_id,
                             self.community_right.run_time)
                _LOGGER.info("right together  id %d , run_time %d ", right_together_variation.graph_id,
                             right_together_variation.run_time)
                _LOGGER.info("right split     id %d , run_time %d ", right_split_variation.graph_id,
                             right_split_variation.run_time)

                # 取run_time最小的作为新一轮变异的群落
                self.community_left = self.getSurviveCommunity(self.community_left, left_together_variation,
                                                               left_split_variation)
                # 取run_time最小的作为新一轮变异的群落
                self.community_right = self.getSurviveCommunity(self.community_right, right_together_variation,
                                                                right_split_variation)

            iter_end = time.time()
            iterations = iterations + 1

            _LOGGER.info("----------------------------------")
            _LOGGER.info("iterations: %d / %d, spend time %d s",
                         iterations, max_iterations, iter_end - iter_start)

            if self.community_left.graph_id == best_left_id and self.community_right.graph_id == best_right_id:
                if same_time == max_same_time:
                    break
                same_time += 1
            else:
                best_left_id = self.community_left.graph_id
                best_right_id = self.community_right.graph_id
                same_time = 0

            _LOGGER.info("same time: %d / %d", same_time, max_same_time)

            if iterations == max_iterations:
                break

        evolution_end = time.time()
        _LOGGER.info("evolution total spend %d s, The number of iterations: %d",
                     evolution_end - evolution_start, iterations)

        _LOGGER.info(str(self.community_left.device_used_memory_lst))
        _LOGGER.info(str(self.community_right.device_used_memory_lst))
        _LOGGER.info(str(self.topo_community.device_used_memory_lst))
        _LOGGER.info(str(self.sct_community.device_used_memory_lst))
        _LOGGER.info(str(self.etf_community.device_used_memory_lst))

    def get_op_memory(self, op_data):
        return op_data["temporary_memory"] + sum(op_data["output_memory"])

    def update_device_memory_info(self, genetic_graph, target_op, new_target_device):
        community_op_graph_ops_dict = genetic_graph.graph.nodes()._nodes
        # 获取目标算子
        op_data = community_op_graph_ops_dict[target_op]
        # 获取目标算子的内存开销：算子内存 + 张量内存大小
        op_memory = self.get_op_memory(op_data)
        # 更新集群的内存信息
        if 'p' not in op_data:
            # 如果当前算是第一次放置
            genetic_graph.device_used_memory_lst[new_target_device] += op_memory
        else:
            old_target_device = community_op_graph_ops_dict[target_op]['p']
            # 如果当前算子不是第一次放置，则需要进行内存的转移
            genetic_graph.device_used_memory_lst[old_target_device] -= op_memory
            genetic_graph.device_used_memory_lst[new_target_device] += op_memory

    # 为群落指定分配设备
    def change_graph_placement(self, genetic_graph, target_op, target_device):
        # 更新内存
        self.update_device_memory_info(genetic_graph, target_op, new_target_device=target_device)
        # 指定一个新目标设备
        community_op_graph_ops_dict = genetic_graph.graph.nodes()._nodes
        community_op_graph_ops_dict[target_op]['p'] = target_device
        # 将同组的设备一起调度
        co_group = community_op_graph_ops_dict[target_op]['colocation_group']
        if len(self.colocation_group[co_group]['ops']) > 1:
            for group_member_ops in self.colocation_group[co_group]['ops']:
                ops_id = group_member_ops['id']
                # 更新内存
                self.update_device_memory_info(genetic_graph, ops_id, new_target_device=target_device)
                # 指定新目标设备
                community_op_graph_ops_dict[ops_id]['p'] = target_device

        # 检查是否发生OOM
        for device_id, used_memory in enumerate(genetic_graph.device_used_memory_lst):
            # 说明发生了OOM不能这样放置
            if self.device_graph_dict[device_id]['memory_limit'] < used_memory:
                return False
        return True

    # 初始化群落
    def init_graph_community(self, op_graph):
        single_gpu = copy.deepcopy(op_graph)
        single_community = GeneticGraph(single_gpu, self.getGraphId(), device_num=self.device_num)
        self.initial_graph_placement(single_community, "single_gpu")
        self.single = single_gpu

        self.community_left = GeneticGraph(copy.deepcopy(op_graph), self.getGraphId(), device_num=self.device_num)
        self.initial_graph_placement(self.community_left, "random_gpu")

        self.community_right = GeneticGraph(copy.deepcopy(op_graph), self.getGraphId(), device_num=self.device_num)
        self.initial_graph_placement(self.community_right, "random_gpu")

        self.topo_graph = self.topo(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))
        self.sct_graph = self.sct(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))
        self.etf_graph = self.etf(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))

        self.topo_community = GeneticGraph(self.topo_graph, self.getGraphId(), device_num=self.device_num)
        self.sct_community = GeneticGraph(self.topo_graph, self.getGraphId(), device_num=self.device_num)
        self.etf_community = GeneticGraph(self.topo_graph, self.getGraphId(), device_num=self.device_num)

        self.topo_run_time = self.caculate_run_time(self.topo_community)
        self.sct_run_time = self.caculate_run_time(self.sct_community)
        self.eft_run_time = self.caculate_run_time(self.etf_community)
        self.single_run_time = self.caculate_run_time(single_community)



        self.topo_community.initDeviceUsedMemoryLst()
        self.sct_community.initDeviceUsedMemoryLst()
        self.etf_community.initDeviceUsedMemoryLst()

        _LOGGER.info("init total %d graph community finished", len(self.graph_communities))

    def caculateTotalOutputMemory(self, graph):
        totalOutputMemory = 0
        op_graph_ops_dict = graph.nodes()._nodes
        for parent_id, parent_node in op_graph_ops_dict.items():
            descend_ids = graph.successors(parent_id)
            for succ_id in descend_ids:
                if len(parent_node['output_memory']) > 0:
                    if op_graph_ops_dict[succ_id]['p'] != parent_node['p']:
                        totalOutputMemory += parent_node['output_memory'][0]
        return totalOutputMemory

    # placeto的模拟器 + 线性拟合的通信代价 作为 run_time
    def caculate_run_time(self, community):
        graph = community.graph
        ungrouped_pl = {}
        op_graph_ops_dict = graph.nodes()._nodes

        for op in self.mg.node:
            ungrouped_pl[op.name] = 0

        for id, data in op_graph_ops_dict.items():
            ungrouped_pl[data['name']] = data['p']

        for op_name, device in ungrouped_pl.items():
            if device == -1:
                ungrouped_pl[op_name] = 0
                for id, data in op_graph_ops_dict.items():
                    if data['name'] == op_name:
                        data['p'] = 0
                        break
        run_time, start_times = self.simulator.simulate(ungrouped_pl, sim_mem_usage=True)

        memory = self.caculateTotalOutputMemory(graph)

        if community.is_OOM == True:
            # 如果当前的策略出现了OOM， 则给他一个很大的权重（不让他参与下一轮的迭代）
            run_time = run_time * 1000

        # _LOGGER.info("run_time %d transe_time %lf", run_time, self.comm_cost_fn(memory))
        return run_time + self.comm_cost_fn(memory) * 0.5
        # return self.comm_cost_fn(memory)

    # 这个是baechi自带的模拟器
    def caculate_run_time1(self, graph):
        device_graph = copy.deepcopy(self.device_graph)
        scheduler = VirtualScheduler(graph, device_graph)
        scheduler.initialize()
        run_time = scheduler.run()
        return run_time

    def __init__(self, op_graph, device_graph, initial_fun, cost_dict, comm_cost_fn):
        # 进化群落的大小设为15
        self.community_size = 2
        self.graph_communities = []
        self.comm_cost_fn = comm_cost_fn

        self.graph_nodes_num = len(op_graph.nodes()._nodes)
        # 初始化的搜索方法
        self.topo = initial_fun[0]
        self.sct = initial_fun[1]
        self.etf = initial_fun[2]
        self.single = None

        self.colocation_group = placer_utils.create_colocation_group_infos(op_graph)

        self.device_graph = device_graph
        self.device_graph_dict = device_graph.nodes()._nodes
        self.device_num = len(self.device_graph_dict)
        self.device_index = range(self.device_num)


        self.device_names = ['/device:GPU:%d' % i for i in range(self.device_num)]
        self.cost_dict = cost_dict
        self.mg = self.cost_dict['graphdef']
        self.op_perf = self.cost_dict['op_perf']
        self.step_stats = self.cost_dict['step_stats']

        self.topo_graph = None
        self.sct_graph = None
        self.etf_graph = None
        self.community_left = None
        self.community_right = None

        # 标记OOM后重新找的次数，当到达一定的次数时不再重新搜索，迭代直接结束
        self.refind_times = 0

        # 获取张量字典
        self.ops_transfer_tensor = {}
        self.total_tensor_memory = 0

        # 执行模拟器
        self.simulator = ImportantOpsSimulator(self.mg, self.op_perf, self.step_stats, self.device_names)

        op_graph_ops_dict = op_graph.nodes()._nodes
        for parent_id, parent_node in op_graph_ops_dict.items():
            descend_ids = op_graph.successors(parent_id)
            if len(parent_node['output_memory']) > 0:
                for succ_id in descend_ids:
                    pair = str(parent_id) + "_" + str(succ_id)
                    self.ops_transfer_tensor[pair] = parent_node['output_memory'][0]
                    self.total_tensor_memory += parent_node['output_memory'][0]
        self.ops_transfer_tensor = sorted(self.ops_transfer_tensor.items(), key=lambda item: item[1], reverse=True)

        self.graph_id = 0
        # 初始化
        self.init_graph_community(op_graph=op_graph)

        self.graph_dict = {}

