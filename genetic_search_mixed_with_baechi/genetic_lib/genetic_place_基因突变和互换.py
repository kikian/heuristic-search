import copy
import random
from utils import logger
from placer import placer_utils
import time
from sim.tf_placement_sim.tf_pl_simulator import ImportantOpsSimulator
from genetic_lib.genetic_graph import GeneticGraph

_LOGGER = logger.get_logger(__file__, level=logger.INFO)

class Genetic_placer:

    # 为群落随机分配设备
    # init_type
    def initial_graph_placement(self, op_graph, init_type):
        device_num = len(self.device_graph.nodes())
        ops_count = 0
        op_graph_ops_dict = op_graph.nodes()._nodes
        for id, data in op_graph_ops_dict.items():
            # target_device = random.randint(0, device_num-1)
            if init_type == "random_gpu":
                target_device = random.randint(0, device_num - 1)
            elif init_type == "single_gpu":
                target_device = 0
            # 如果没有放置过
            if 'p' not in data:
                # 随机指定一个目标设备
                data['p'] = target_device
                # _LOGGER.info("[placement] %s: %d", data["name"], data["p"])
                # 将同组的设备一起调度
                co_group = data['colocation_group']
                ops_count = ops_count + 1
                if len(self.colocation_group[co_group]['ops']) > 1:
                    for group_member_ops in self.colocation_group[co_group]['ops']:
                        ops_id = group_member_ops['id']
                        if 'p' not in op_graph_ops_dict[ops_id]:
                            op_graph_ops_dict[ops_id]['p'] = target_device
                            # _LOGGER.info("[placement] %s: %d", op_graph_ops_dict[ops_id]["name"],op_graph_ops_dict[ops_id]["p"])
                            ops_count = ops_count + 1

        _LOGGER.info("initial finished total: %d / %d", ops_count, len(op_graph_ops_dict))

    def genesSwap(self):
        # 后面的3/4的 群落和 前1/4的群落进行基因互换
        good_commity_num = int(self.community_size / 4)
        bad_commity_num = int(good_commity_num * 3)
        new_swap_group = []

        for i in range(bad_commity_num):
            # 判断是否发生变化
            chang_flag = False
            # 当前接收基因的群落
            bad_commity_index = i + good_commity_num
            bad_commity = self.graph_communities[bad_commity_index].graph
            bad_commity_ops_dict = bad_commity.nodes()._nodes

            new_bad_community = copy.deepcopy(bad_commity)
            # 从好的群落里面随机选择一个
            good_commity_index = random.randint(0, good_commity_num - 1)
            good_commity = self.graph_communities[good_commity_index].graph
            good_commity_ops_dict = good_commity.nodes()._nodes
            # 从好群落里面随机选择3个基因替换给坏群落
            for j in range(5):
                # ops_transfer_tensor前30%的算子对通信开销较大
                big_tensor_range = int(len(self.ops_transfer_tensor) * 0.3)
                # 70%的概率从前30%的算子对里面拿
                rand = random.random() * 1
                if rand < 0.7:
                    target_op_pair = random.randint(0, big_tensor_range - 1)
                else:
                    # 30%的概率从后面70%的算子对中拿
                    target_op_pair = random.randint(big_tensor_range, len(self.ops_transfer_tensor) - 1)
                ops_pair = self.ops_transfer_tensor[target_op_pair][0].split('_')
                from_ops = int(ops_pair[0])
                to_ops = int(ops_pair[1])
                from_ops_target = good_commity_ops_dict[from_ops]['p']
                to_ops_target = good_commity_ops_dict[to_ops]['p']

                if from_ops_target != bad_commity_ops_dict[from_ops]['p']:
                    self.change_graph_placement(new_bad_community.nodes()._nodes, from_ops, from_ops_target)
                    chang_flag = True
                if to_ops_target != bad_commity_ops_dict[to_ops]['p']:
                    self.change_graph_placement(new_bad_community.nodes()._nodes, to_ops, to_ops_target)
                    chang_flag = True
            # 如果群落发生了变化 则表示出现了新群落
            if chang_flag:
                new_swap_group.append(GeneticGraph(bad_commity, self.graph_id))
                self.graph_id += 1
        return new_swap_group

    # 随机选择一对算子，生成将其拆分和放一起的两种状态图
    def genesVariation(self):
        device_num = len(self.device_graph.nodes())
        new_variation_group = []
        for index in range(int(self.community_size / 2)):
            op_graph = self.graph_communities[index].graph
            op_graph_ops_dict = op_graph.nodes()._nodes
            # ops_transfer_tensor前30%的算子对通信开销较大
            big_tensor_range = int(len(self.ops_transfer_tensor) * 0.3)

            change_flag_together = False
            change_flag_split = False
            together_community = copy.deepcopy(op_graph)
            split_community = copy.deepcopy(op_graph)

            # for j in range(2):
            # 70%的概率从前30%的算子对里面拿
            rand = random.random() * 1
            if rand < 0.7:
                target_op_pair = random.randint(0, big_tensor_range - 1)
            else:
                # 30%的概率从后面70%的算子对中拿
                target_op_pair = random.randint(big_tensor_range, len(self.ops_transfer_tensor) - 1)
            ops_pair = self.ops_transfer_tensor[target_op_pair][0].split('_')
            from_ops = int(ops_pair[0])
            to_ops = int(ops_pair[1])

            # 将这两个算子放一起
            if op_graph_ops_dict[from_ops]['p'] != op_graph_ops_dict[to_ops]['p']:
                # 这里可以做一个负载均衡的选择
                target_device = random.randint(0, device_num - 1)
                self.change_graph_placement(together_community.nodes()._nodes, from_ops, target_device)
                self.change_graph_placement(together_community.nodes()._nodes, to_ops, target_device)
                change_flag_together = True
            # 将这两个算子分开放
            if op_graph_ops_dict[from_ops]['p'] == op_graph_ops_dict[to_ops]['p']:
                device_range = range(device_num)
                # 随机选择两个不一样的设备
                # 这里可以做一个负载均衡的选择
                target_device1, target_device2 = random.sample(device_range, 2)
                self.change_graph_placement(split_community.nodes()._nodes, from_ops, target_device1)
                self.change_graph_placement(split_community.nodes()._nodes, to_ops, target_device2)
                change_flag_split = True
                # new_variation_group.append(GeneticGraph(split_community, self.graph_id))
                # self.graph_id += 1
        if change_flag_together:
            new_variation_group.append(GeneticGraph(together_community, self.graph_id))
            self.graph_id += 1
        if change_flag_split:
            new_variation_group.append(GeneticGraph(split_community, self.graph_id))
            self.graph_id += 1

        return new_variation_group

    def caculateTotalOutputMemory(self, graph):
        totalOutputMemory = 0
        op_graph_ops_dict = graph.nodes()._nodes
        for parent_id, parent_node in op_graph_ops_dict.items():
            descend_ids = graph.successors(parent_id)
            for succ_id in descend_ids:
                if len(parent_node['output_memory']) > 0:
                    if op_graph_ops_dict[succ_id]['p'] != parent_node['p']:
                        # assert len(op_info['output_memory']) == 1
                        # if len(op_info['output_memory']) > 0:
                        # totalOutputMemory += op_info['output_memory'][0]
                        totalOutputMemory += parent_node['output_memory'][0]
        return totalOutputMemory

    # 群落进化迭代
    def evolutionOfCommunity(self):
        evolution_start = time.time()
        max_iterations = 200
        iterations = 0
        # index_lst 作为下标，方便高效打乱
        index_lst = []
        for i in range(len(self.graph_communities)):
            index_lst.append(i)

        top1 = self.graph_communities[0].graph_id
        top2 = self.graph_communities[1].graph_id
        top3 = self.graph_communities[2].graph_id
        same_time = 0
        while True:
            iter_start = time.time()
            # 前8个基因变异
            new_variation_group = self.genesVariation()
            # 后12个和前面4个基因互换
            new_swap_group = self.genesSwap()

            _LOGGER.info("new_variation_group size: %d", len(new_variation_group))
            _LOGGER.info("new_swap_group size: %d", len(new_swap_group))

            self.graph_communities.extend(new_variation_group)
            self.graph_communities.extend(new_swap_group)

            for index, genetic_graph in enumerate(self.graph_communities):
                # 如果已经计算过run_time则无需重新计算
                if genetic_graph.run_time != None:
                    continue

                graph = genetic_graph.graph
                ungrouped_pl = {}
                op_graph_ops_dict = graph.nodes()._nodes

                for op in self.mg.node:
                    ungrouped_pl[op.name] = 0

                for id, data in op_graph_ops_dict.items():
                    ungrouped_pl[data['name']] = data['p']

                for op_name, device in ungrouped_pl.items():
                    if device == -1:
                        ungrouped_pl[op_name] = 0
                        for id, data in op_graph_ops_dict.items():
                            if data['name'] == op_name:
                                data['p'] = 0
                                break

                run_time, start_times = self.simulator.simulate(ungrouped_pl, sim_mem_usage=True)
                # output_memory = self.caculateTotalOutputMemory(graph)
                genetic_graph.run_time = run_time
                # genetic_graph.outputMemory = output_memory

            # simulate_res_lst.sort(key=lambda simulate_res: simulate_res[1])
            self.graph_communities.sort(key=lambda my_genetic_graph: my_genetic_graph.run_time)
            # self.graph_communities.sort(key=lambda my_genetic_graph: my_genetic_graph.outputMemory)

            new_graph_communities = []
            for genetic_graph in self.graph_communities:
                if len(new_graph_communities) < self.community_size:
                    new_graph_communities.append(genetic_graph)
                if len(new_graph_communities) < 5:
                    now_genetic_graph = genetic_graph
                    _LOGGER.info("graph_id %d , run_time: %d ", now_genetic_graph.graph_id, now_genetic_graph.run_time)
                    # _LOGGER.info("graph_id %d , run_time: %d output_memory %d", now_genetic_graph.graph_id, now_geneti
                    # c_graph.run_time, now_genetic_graph.outputMemory)

            self.graph_communities = new_graph_communities
            iter_end = time.time()
            iterations = iterations + 1
            _LOGGER.info(" iterations: %d / %d, spend time %d s",
                         iterations, max_iterations, iter_end - iter_start)

            now_top1 = self.graph_communities[0].graph_id
            now_top2 = self.graph_communities[1].graph_id
            now_top3 = self.graph_communities[2].graph_id

            # 结束条件1：到达指定的迭代次数
            if iterations == max_iterations:
                break

            # 结束条件2：策略收敛
            if now_top1 == top1 and now_top2 == top2 and now_top3 == top3:
                # 如果top3连续10轮没有变化则说明收敛了
                if same_time == 5:
                    break
                same_time += 1
            else:
                top1 = now_top1
                top2 = now_top2
                top3 = now_top3
                same_time = 0

        evolution_end = time.time()

        _LOGGER.info("evolution total spend %d ms, The number of iterations: %d",
                     evolution_end - evolution_start, iterations)



    # 为群落指定分配设备
    def change_graph_placement(self, community_op_graph_ops_dict, target_op, target_device):
        # 随机指定一个目标设备
        community_op_graph_ops_dict[target_op]['p'] = target_device
        # 将同组的设备一起调度
        co_group = community_op_graph_ops_dict[target_op]['colocation_group']
        if len(self.colocation_group[co_group]['ops']) > 1:
            for group_member_ops in self.colocation_group[co_group]['ops']:
                ops_id = group_member_ops['id']
                community_op_graph_ops_dict[ops_id]['p'] = target_device

    # 为群落随机分配设备
    def random_change_graph_placement(self, op_graph):
        device_num = len(self.device_graph.nodes())
        op_graph_ops_dict = op_graph.nodes()._nodes

        stop_flag = False
        while not stop_flag:
            # 随机选择一个算子
            target_op = random.randint(0, len(op_graph_ops_dict) - 1)
            # 随机选择一个设备
            target_device = random.randint(0, device_num - 1)
            # 如果当前选择的设备和之前一样则重新
            if op_graph_ops_dict[target_op]['p'] == target_device:
                continue
            else:
                stop_flag = True

            # 指定一个目标设备
            self.change_graph_placement(op_graph_ops_dict, target_op, target_device)


    # 从三种启发式算法的基础上进行搜索
    def init_graph_community_from_beachi(self, op_graph):

        self.topo_graph = self.topo(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))
        self.sct_graph = self.sct(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))
        self.etf_graph = self.etf(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))

        # 三种启发式算法的策略图
        self.graph_communities.append(copy.deepcopy(self.topo_graph))
        self.graph_communities.append(copy.deepcopy(self.sct_graph))
        self.graph_communities.append(copy.deepcopy(self.etf_graph))
        new_community = copy.deepcopy(op_graph)
        self.initial_graph_placement(new_community, "single_gpu")
        self.graph_communities.append(new_community)
        self.graph_communities.append(copy.deepcopy(self.etf_graph))

        return
        # 在这三种策略图的基础上随机变异 生成新的群落
        index = 3
        while len(self.graph_communities) < self.community_size:
            if index % 3 == 0:
                # topo_graph
                new_community = copy.deepcopy(self.topo_graph)
                self.random_change_graph_placement(new_community)
                self.graph_communities.append(new_community)
            elif index % 3 == 1:
                # sct_graph
                new_community = copy.deepcopy(self.sct_graph)
                self.random_change_graph_placement(new_community)
                self.graph_communities.append(new_community)
            else:
                # etf_graph
                new_community = copy.deepcopy(self.etf_graph)
                self.random_change_graph_placement(new_community)
                self.graph_communities.append(new_community)
            index = index + 1

        _LOGGER.info("init total %d graph community finished", len(self.graph_communities))

    # 初始化群落
    def init_graph_community(self, op_graph):

        self.topo_graph = self.topo(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))
        self.sct_graph = self.sct(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))
        self.etf_graph = self.etf(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))


        single_gpu = copy.deepcopy(op_graph)
        self.initial_graph_placement(single_gpu, "single_gpu")
        self.single = single_gpu

        while len(self.graph_communities) < self.community_size:
            new_community = copy.deepcopy(op_graph)
            self.initial_graph_placement(new_community, "random_gpu")
            # 加入列表中
            self.graph_communities.append(GeneticGraph(new_community, self.graph_id))
            self.graph_id += 1

        _LOGGER.info("init total %d graph community finished", len(self.graph_communities))

    def __init__(self, op_graph, device_graph, initial_fun, cost_dict):
        # 进化群落的大小设为15
        self.community_size = 16
        self.graph_communities = []

        self.graph_nodes_num = len(op_graph.nodes()._nodes)
        # 初始化的搜索方法
        self.topo = initial_fun[0]
        self.sct = initial_fun[1]
        self.etf = initial_fun[2]
        self.single = None

        self.colocation_group = placer_utils.create_colocation_group_infos(op_graph)
        #
        # _LOGGER.info(len(self.colocation_group))

        self.device_graph = device_graph

        self.topo_graph = None
        self.sct_graph = None
        self.etf_graph = None

        # 获取张量字典
        self.ops_transfer_tensor = {}
        self.total_tensor_memory = 0

        op_graph_ops_dict = op_graph.nodes()._nodes
        for parent_id, parent_node in op_graph_ops_dict.items():
            descend_ids = op_graph.successors(parent_id)
            if len(parent_node['output_memory']) > 0:
                for succ_id in descend_ids:
                    pair = str(parent_id) + "_" + str(succ_id)
                    self.ops_transfer_tensor[pair] = parent_node['output_memory'][0]
                    self.total_tensor_memory += parent_node['output_memory'][0]
        self.ops_transfer_tensor = sorted(self.ops_transfer_tensor.items(), key=lambda item: item[1], reverse=True)

        self.graph_id = 0
        # 初始化
        self.init_graph_community(op_graph=op_graph)
        # self.init_graph_community_from_beachi(op_graph=op_graph)

        self.device_names = ['/device:GPU:%d' % i for i in range(6)]
        self.cost_dict = cost_dict
        self.mg = self.cost_dict['graphdef']
        self.op_perf = self.cost_dict['op_perf']
        self.step_stats = self.cost_dict['step_stats']

        self.simulator = ImportantOpsSimulator(self.mg, self.op_perf, self.step_stats, self.device_names)

        self.graph_dict = {}

