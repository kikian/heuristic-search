import copy

# 假设一：算子的放置只影响临近节点的性能
class GeneticGraph:
    def __init__(self, graph, id, survive_genes=None, device_used_memory_lst=None ,device_num=None):
        self.graph = graph
        self.run_time = None
        self.outputMemory = None
        self.graph_id = id
        self.device_num = device_num
        self.is_OOM = False

        # 好基因
        if survive_genes != None:
            self.survive_genes = copy.deepcopy(survive_genes)
        else:
            self.survive_genes = {}

        if device_used_memory_lst != None:
            self.device_used_memory_lst = copy.deepcopy(device_used_memory_lst)
        else:
            # 当前集群的设备
            self.device_used_memory_lst = []
            assert self.device_num is not None
            for i in range(self.device_num):
                self.device_used_memory_lst.append(0)

    # 初始化集群的设备负载信息
    def initDeviceUsedMemoryLst(self):
        for id, ops in self.graph.nodes()._nodes.items():
            self.device_used_memory_lst[ops['p']] += ops["temporary_memory"] + sum(ops["output_memory"])
