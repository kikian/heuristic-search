import copy
import random
from utils import logger
from placer import placer_utils
import time
from sim.tf_placement_sim.tf_pl_simulator import ImportantOpsSimulator
from genetic_lib.genetic_graph import GeneticGraph
from placer.virtual_scheduler import VirtualScheduler

_LOGGER = logger.get_logger(__file__, level=logger.INFO)

class Genetic_placer:

    # 为群落随机分配设备
    # init_type
    def initial_graph_placement(self, op_graph, init_type):
        device_num = len(self.device_graph.nodes())
        ops_count = 0
        op_graph_ops_dict = op_graph.nodes()._nodes
        for id, data in op_graph_ops_dict.items():
            # target_device = random.randint(0, device_num-1)
            if init_type == "random_gpu":
                target_device = random.randint(0, device_num - 1)
            elif init_type == "single_gpu":
                target_device = 0
            # 如果没有放置过
            if 'p' not in data:
                # 随机指定一个目标设备
                data['p'] = target_device
                # _LOGGER.info("[placement] %s: %d", data["name"], data["p"])
                # 将同组的设备一起调度
                co_group = data['colocation_group']
                ops_count = ops_count + 1
                if len(self.colocation_group[co_group]['ops']) > 1:
                    for group_member_ops in self.colocation_group[co_group]['ops']:
                        ops_id = group_member_ops['id']
                        if 'p' not in op_graph_ops_dict[ops_id]:
                            op_graph_ops_dict[ops_id]['p'] = target_device
                            # _LOGGER.info("[placement] %s: %d", op_graph_ops_dict[ops_id]["name"],op_graph_ops_dict[ops_id]["p"])
                            ops_count = ops_count + 1

        _LOGGER.info("initial finished total: %d / %d", ops_count, len(op_graph_ops_dict))

    def caculateTotalOutputMemory(self, graph):
        totalOutputMemory = 0
        op_graph_ops_dict = graph.nodes()._nodes
        for parent_id, parent_node in op_graph_ops_dict.items():
            descend_ids = graph.successors(parent_id)
            for succ_id in descend_ids:
                if len(parent_node['output_memory']) > 0:
                    if op_graph_ops_dict[succ_id]['p'] != parent_node['p']:
                        # assert len(op_info['output_memory']) == 1
                        # if len(op_info['output_memory']) > 0:
                        # totalOutputMemory += op_info['output_memory'][0]
                        totalOutputMemory += parent_node['output_memory'][0]
        return totalOutputMemory

    def genesVariation(self, community):
        device_num = len(self.device_graph.nodes())
        # 当前群落的基因图
        op_graph = community.graph
        op_graph_ops_dict = op_graph.nodes()._nodes
        # ops_transfer_tensor前30%的算子对通信开销较大
        big_tensor_range = int(len(self.ops_transfer_tensor) * 0.5)


        count = 0
        while True:
            # todo: 这里可能会死循环
            # 70%的概率从前20%的算子对里面拿
            rand = random.random() * 1
            if rand < 0.5:
                target_op_pair = random.randint(0, big_tensor_range - 1)
            else:
                # 30%的概率从后面70%的算子对中拿
                target_op_pair = random.randint(big_tensor_range, len(self.ops_transfer_tensor) - 1)
            pair_key = self.ops_transfer_tensor[target_op_pair][0]
            # 只改变没改变过的基因
            if not community.survive_genes.__contains__(pair_key):
                ops_pair = pair_key.split('_')
                from_ops = int(ops_pair[0])
                to_ops = int(ops_pair[1])
                break
            count += 1
            if count == 10000:
                community.survive_genes = {}

        # 表示该基因存活
        community.survive_genes[pair_key] = 1
        survive_genes_together = copy.deepcopy(community.survive_genes)
        survive_genes_split = copy.deepcopy(community.survive_genes)

        new_graph_together = copy.deepcopy(op_graph)
        new_graph_split = copy.deepcopy(op_graph)

        # 更新父群落的存活基因
        # if community.survive_genes.__contains__(pair_key):
        #     community.survive_genes[pair_key] += 1
        # else:

        # 如果这两个算子已经放在一起了 则换一个设备放一起
        if op_graph_ops_dict[from_ops]['p'] == op_graph_ops_dict[to_ops]['p']:
            target_device = (op_graph_ops_dict[from_ops]['p'] + 1) % device_num
            self.change_graph_placement(new_graph_together.nodes()._nodes, from_ops, target_device)
            self.change_graph_placement(new_graph_together.nodes()._nodes, to_ops, target_device)
            # 恢复存活基因的计数
            # survive_genes_together[pair_key] = 1
        else:
            # 如果这两个算子已经分开放了 则换一对设备分开放
            target_device1 = (op_graph_ops_dict[from_ops]['p'] + 1) % device_num
            target_device2 = (op_graph_ops_dict[to_ops]['p'] + 1) % device_num
            self.change_graph_placement(new_graph_split.nodes()._nodes, from_ops, target_device1)
            self.change_graph_placement(new_graph_split.nodes()._nodes, to_ops, target_device2)
            # 恢复存活基因的计数
            # survive_genes_split[pair_key] = 1

        return GeneticGraph(new_graph_together, self.getGraphId(), survive_genes_together), \
               GeneticGraph(new_graph_split, self.getGraphId(), survive_genes_split),

    def genesSwap(self, community_left, community_right):
        left_survive_genes = community_left.survive_genes
        right_survive_genes = community_right.survive_genes
        #
        community_left_ops_dict = community_left.graph.nodes()._nodes
        community_right_ops_dict = community_right.graph.nodes()._nodes
        # 生成新的基因群落
        # new_left_community = GeneticGraph(copy.deepcopy(community_left.graph), self.getGraphId(), left_survive_genes)
        # new_right_community = GeneticGraph(copy.deepcopy(community_right.graph), self.getGraphId(), right_survive_genes)

        new_left_community = GeneticGraph(copy.deepcopy(community_left.graph), self.getGraphId())
        new_right_community = GeneticGraph(copy.deepcopy(community_right.graph), self.getGraphId())
        # 将两边的基因优质相互交换
        new_left_graph = new_left_community.graph
        new_right_graph = new_right_community.graph
        # 将community_left的优质基因给new_right_community
        for pair_key, survive_time in left_survive_genes.items():
            # if survive_time > 0:
            ops_pair = pair_key.split('_')
            from_ops = int(ops_pair[0])
            to_ops = int(ops_pair[1])
            # 如果存活次数大于2则是优质基因
            self.change_graph_placement(new_right_graph.nodes()._nodes, from_ops,
                                        community_left_ops_dict[from_ops]['p'])
            self.change_graph_placement(new_right_graph.nodes()._nodes, to_ops,
                                        community_left_ops_dict[to_ops]['p'])
            # new_left_community.survive_genes[pair_key] = survive_time

        # 将community_right_ops_dict的优质基因给new_left_community
        for pair_key, survive_time in right_survive_genes.items():
            # if survive_time > 0:
            ops_pair = pair_key.split('_')
            from_ops = int(ops_pair[0])
            to_ops = int(ops_pair[1])
            # 如果存活次数大于2则是优质基因
            self.change_graph_placement(new_left_graph.nodes()._nodes, from_ops,
                                        community_right_ops_dict[from_ops]['p'])
            self.change_graph_placement(new_left_graph.nodes()._nodes, to_ops,
                                        community_right_ops_dict[to_ops]['p'])
            # new_right_community.survive_genes[pair_key] = survive_time

        return new_left_community, new_right_community


    def getGraphId(self):
        self.graph_id += 1
        return self.graph_id

    def getSurviveCommunity(self, community1, community2, community3):
        if community1.run_time <= community2.run_time:
            survive_community = community1
        else:
            survive_community = community2
        if survive_community.run_time <= community3.run_time:
            return survive_community
        return community3


    def getSurviveCommunityTwo(self, community1, community2):
        if community1.run_time <= community2.run_time:
            return community1
        else:
            return community2



    def evolutionOfCommunity(self):
        evolution_start = time.time()
        max_iterations = 500
        iterations = 0

        # community_left = self.community_left
        # community_right = self.community_right

        self.community_left.run_time = self.caculate_run_time(self.community_left.graph)
        self.community_right.run_time = self.caculate_run_time(self.community_right.graph)

        _LOGGER.info("topo graph , run_time: %d ", self.topo_run_time)
        _LOGGER.info("sct graph , run_time: %d ", self.sct_run_time)
        _LOGGER.info("etf graph , run_time: %d ", self.eft_run_time)
        _LOGGER.info("single , run_time: %d ", self.single_run_time)

        best_left_id = self.community_left.graph_id
        best_right_id = self.community_right.graph_id
        same_time = 0
        max_same_time = 200
        while True:
            _LOGGER.info("----------------------------------")
            iter_start = time.time()
            if (iterations + 1) % 5 == 0:
                # 两个群落交换优质基因
                _LOGGER.info("genesSwap ...... ")
                new_left_community, new_right_community = self.genesSwap(self.community_left, self.community_right)
                # 计算时间
                new_left_community.run_time = self.caculate_run_time(new_left_community.graph)
                new_right_community.run_time = self.caculate_run_time(new_right_community.graph)

                _LOGGER.info("left community  id %d , run_time %d ", self.community_left.graph_id, self.community_left.run_time)
                _LOGGER.info("left swap       id %d , run_time %d ", new_left_community.graph_id, new_left_community.run_time)
                _LOGGER.info("right community id %d , run_time %d ", self.community_right.graph_id, self.community_right.run_time)
                _LOGGER.info("right swap      id %d , run_time %d ", new_right_community.graph_id, new_right_community.run_time)

                # 计算存活的群落
                self.community_left = self.getSurviveCommunityTwo(self.community_left, new_left_community)
                self.community_right = self.getSurviveCommunityTwo(self.community_right, new_right_community)
            else:
                _LOGGER.info("genesVariation ...... ")
                # 两个竞争群落各自变异
                # communityLeft变异
                left_together_variation, left_split_variation = self.genesVariation(self.community_left)
                left_together_variation.run_time = self.caculate_run_time(left_together_variation.graph)
                left_split_variation.run_time = self.caculate_run_time(left_split_variation.graph)

                # 取run_time最小的作为新一轮变异的群落
                self.community_left = self.getSurviveCommunity(self.community_left, left_together_variation, left_split_variation)
                # communityRight变异
                right_together_variation, right_split_variation = self.genesVariation(self.community_right)
                right_together_variation.run_time = self.caculate_run_time(right_together_variation.graph)
                right_split_variation.run_time = self.caculate_run_time(right_split_variation.graph)

                _LOGGER.info("left community  id %d , run_time %d ", self.community_left.graph_id,
                             self.community_left.run_time)
                _LOGGER.info("left together   id %d , run_time %d ", left_together_variation.graph_id,
                             left_together_variation.run_time)
                _LOGGER.info("left split      id %d , run_time %d ", left_split_variation.graph_id,
                             left_split_variation.run_time)
                _LOGGER.info("right community id %d , run_time %d ", self.community_right.graph_id,
                             self.community_right.run_time)
                _LOGGER.info("right together  id %d , run_time %d ", right_together_variation.graph_id,
                             right_together_variation.run_time)
                _LOGGER.info("right split     id %d , run_time %d ", right_split_variation.graph_id,
                             right_split_variation.run_time)

                # 取run_time最小的作为新一轮变异的群落
                self.community_left = self.getSurviveCommunity(self.community_left, left_together_variation,
                                                               left_split_variation)
                # 取run_time最小的作为新一轮变异的群落
                self.community_right = self.getSurviveCommunity(self.community_right, right_together_variation,
                                                                right_split_variation)

            iter_end = time.time()
            iterations = iterations + 1

            _LOGGER.info("----------------------------------")
            _LOGGER.info("iterations: %d / %d, spend time %d s",
                         iterations, max_iterations, iter_end - iter_start)

            if self.community_left.graph_id == best_left_id and self.community_right.graph_id == best_right_id:
                if same_time == max_same_time:
                    break
                same_time += 1
            else:
                best_left_id = self.community_left.graph_id
                best_right_id = self.community_right.graph_id
                same_time = 0

            _LOGGER.info("same time: %d / %d", same_time, max_same_time)

            if iterations == max_iterations:
                break

        # if community_left.run_time < community_right.run_time:
        #     self.graph_communities[0] = community_left
        # else:
        #     self.graph_communities[0] = community_right

        evolution_end = time.time()
        _LOGGER.info("evolution total spend %d s, The number of iterations: %d",
                     evolution_end - evolution_start, iterations)

        self.community_left.initDeviceUsedMemoryLst()
        self.community_right.initDeviceUsedMemoryLst()
        _LOGGER.info(str(self.community_left.device_used_memory_lst))
        _LOGGER.info(str(self.community_right.device_used_memory_lst))
        _LOGGER.info(str(self.topo_community.device_used_memory_lst))
        _LOGGER.info(str(self.sct_community.device_used_memory_lst))
        _LOGGER.info(str(self.etf_community.device_used_memory_lst))

    # 为群落指定分配设备
    def change_graph_placement(self, community_op_graph_ops_dict, target_op, target_device):
        # 随机指定一个目标设备
        community_op_graph_ops_dict[target_op]['p'] = target_device
        # 将同组的设备一起调度
        co_group = community_op_graph_ops_dict[target_op]['colocation_group']
        if len(self.colocation_group[co_group]['ops']) > 1:
            for group_member_ops in self.colocation_group[co_group]['ops']:
                ops_id = group_member_ops['id']
                community_op_graph_ops_dict[ops_id]['p'] = target_device

    # 为群落随机分配设备
    def random_change_graph_placement(self, op_graph):
        device_num = len(self.device_graph.nodes())
        op_graph_ops_dict = op_graph.nodes()._nodes

        stop_flag = False
        while not stop_flag:
            # 随机选择一个算子
            target_op = random.randint(0, len(op_graph_ops_dict) - 1)
            # 随机选择一个设备
            target_device = random.randint(0, device_num - 1)
            # 如果当前选择的设备和之前一样则重新
            if op_graph_ops_dict[target_op]['p'] == target_device:
                continue
            else:
                stop_flag = True

            # 指定一个目标设备
            self.change_graph_placement(op_graph_ops_dict, target_op, target_device)


    # 从三种启发式算法的基础上进行搜索
    def init_graph_community_from_beachi(self, op_graph):

        self.topo_graph = self.topo(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))
        self.sct_graph = self.sct(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))
        self.etf_graph = self.etf(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))

        # 三种启发式算法的策略图
        self.graph_communities.append(copy.deepcopy(self.topo_graph))
        self.graph_communities.append(copy.deepcopy(self.sct_graph))
        self.graph_communities.append(copy.deepcopy(self.etf_graph))
        new_community = copy.deepcopy(op_graph)
        self.initial_graph_placement(new_community, "single_gpu")
        self.graph_communities.append(new_community)
        self.graph_communities.append(copy.deepcopy(self.etf_graph))

        return
        # 在这三种策略图的基础上随机变异 生成新的群落
        index = 3
        while len(self.graph_communities) < self.community_size:
            if index % 3 == 0:
                # topo_graph
                new_community = copy.deepcopy(self.topo_graph)
                self.random_change_graph_placement(new_community)
                self.graph_communities.append(new_community)
            elif index % 3 == 1:
                # sct_graph
                new_community = copy.deepcopy(self.sct_graph)
                self.random_change_graph_placement(new_community)
                self.graph_communities.append(new_community)
            else:
                # etf_graph
                new_community = copy.deepcopy(self.etf_graph)
                self.random_change_graph_placement(new_community)
                self.graph_communities.append(new_community)
            index = index + 1

        _LOGGER.info("init total %d graph community finished", len(self.graph_communities))

    # 初始化群落
    def init_graph_community(self, op_graph):

        single_gpu = copy.deepcopy(op_graph)
        self.initial_graph_placement(single_gpu, "single_gpu")
        self.single = single_gpu

        self.community_left = GeneticGraph(copy.deepcopy(op_graph), self.getGraphId())
        self.initial_graph_placement(self.community_left.graph, "random_gpu")
        self.community_left.initDeviceUsedMemoryLst()

        self.community_right = GeneticGraph(copy.deepcopy(op_graph), self.getGraphId())
        self.initial_graph_placement(self.community_right.graph, "random_gpu")
        self.community_right.initDeviceUsedMemoryLst()


        self.topo_graph = self.topo(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))
        self.sct_graph = self.sct(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))
        self.etf_graph = self.etf(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))

        self.topo_community = GeneticGraph(self.topo_graph, self.getGraphId())
        self.sct_community = GeneticGraph(self.topo_graph, self.getGraphId())
        self.etf_community = GeneticGraph(self.topo_graph, self.getGraphId())
        self.topo_community.initDeviceUsedMemoryLst()
        self.sct_community.initDeviceUsedMemoryLst()
        self.etf_community.initDeviceUsedMemoryLst()

        _LOGGER.info("init total %d graph community finished", len(self.graph_communities))

    def caculate_run_time(self, graph):
        ungrouped_pl = {}
        op_graph_ops_dict = graph.nodes()._nodes

        for op in self.mg.node:
            ungrouped_pl[op.name] = 0

        for id, data in op_graph_ops_dict.items():
            ungrouped_pl[data['name']] = data['p']

        for op_name, device in ungrouped_pl.items():
            if device == -1:
                ungrouped_pl[op_name] = 0
                for id, data in op_graph_ops_dict.items():
                    if data['name'] == op_name:
                        data['p'] = 0
                        break
        run_time, start_times = self.simulator.simulate(ungrouped_pl, sim_mem_usage=True)
        return run_time

    def caculate_run_time1(self, graph):
        device_graph = copy.deepcopy(self.device_graph)
        scheduler = VirtualScheduler(graph, device_graph)
        scheduler.initialize()
        run_time = scheduler.run()
        return run_time

    # 计算负载均衡
    def caculate_offload(self, graph):
        op_graph_ops_dict = graph.nodes()._nodes
        device_offload_dict = {}
        device_ops_dict = {}
        for id, ops in op_graph_ops_dict.items():
            if not device_offload_dict.__contains__(ops['p']):
                device_offload_dict[ops['p']] = 0
                device_ops_dict[ops['p']] = []
            device_offload_dict[ops['p']] += ops['persistent_memory']
            device_ops_dict[ops['p']].append(id)
        return device_offload_dict, device_ops_dict



    def __init__(self, op_graph, device_graph, initial_fun, cost_dict):
        # 进化群落的大小设为15
        self.community_size = 2 # 16
        self.graph_communities = []

        self.graph_nodes_num = len(op_graph.nodes()._nodes)
        # 初始化的搜索方法
        self.topo = initial_fun[0]
        self.sct = initial_fun[1]
        self.etf = initial_fun[2]
        self.single = None

        self.colocation_group = placer_utils.create_colocation_group_infos(op_graph)
        #
        # _LOGGER.info(len(self.colocation_group))

        self.device_graph = device_graph

        self.topo_graph = None
        self.sct_graph = None
        self.etf_graph = None
        self.community_left = None
        self.community_right = None

        # 获取张量字典
        self.ops_transfer_tensor = {}
        self.total_tensor_memory = 0

        op_graph_ops_dict = op_graph.nodes()._nodes
        for parent_id, parent_node in op_graph_ops_dict.items():
            descend_ids = op_graph.successors(parent_id)
            if len(parent_node['output_memory']) > 0:
                for succ_id in descend_ids:
                    pair = str(parent_id) + "_" + str(succ_id)
                    self.ops_transfer_tensor[pair] = parent_node['output_memory'][0]
                    self.total_tensor_memory += parent_node['output_memory'][0]
        self.ops_transfer_tensor = sorted(self.ops_transfer_tensor.items(), key=lambda item: item[1], reverse=True)

        self.graph_id = 0
        # 初始化
        self.init_graph_community(op_graph=op_graph)
        # self.init_graph_community_from_beachi(op_graph=op_graph)

        self.device_names = ['/device:GPU:%d' % i for i in range(6)]
        self.cost_dict = cost_dict
        self.mg = self.cost_dict['graphdef']
        self.op_perf = self.cost_dict['op_perf']
        self.step_stats = self.cost_dict['step_stats']

        self.simulator = ImportantOpsSimulator(self.mg, self.op_perf, self.step_stats, self.device_names)

        self.graph_dict = {}

        self.topo_run_time = self.caculate_run_time(self.topo_graph)
        self.sct_run_time = self.caculate_run_time(self.sct_graph)
        self.eft_run_time = self.caculate_run_time(self.etf_graph)
        self.single_run_time = self.caculate_run_time(self.single)





