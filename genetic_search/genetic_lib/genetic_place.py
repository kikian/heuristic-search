import copy
import random
from utils import logger
from placer import placer_utils
import time
from sim.tf_placement_sim.tf_pl_simulator import ImportantOpsSimulator
from genetic_lib.genetic_graph import GeneticGraph
from placer.virtual_scheduler import VirtualScheduler
import networkx as nx
import tensorflow as tf
import math


_LOGGER = logger.get_logger(__file__, level=logger.INFO)
FLAGS = tf.app.flags.FLAGS


class Genetic_placer:

    # 为群落随机分配设备
    def initial_graph_placement(self, genetic_graph, init_type):
        assert init_type in ["random_gpu", "single_gpu"]

        op_graph = genetic_graph.graph

        ops_count = 0
        op_graph_ops_dict = op_graph.nodes()._nodes
        for ops_id, data in op_graph_ops_dict.items():
            # 如果没有放置过
            if 'p' not in data:
                # 防止初始化出来的策略就出现了OOM，虽然概率极低
                #while True:
                if init_type == "random_gpu":
                    target_device = random.randint(0, self.device_num - 1)
                elif init_type == "single_gpu":
                    target_device = 0
                # 随机指定一个目标设备
                self.change_graph_placement(genetic_graph, ops_id, target_device)
            ops_count = ops_count + 1

        _LOGGER.info("initial finished total: %d / %d", ops_count, len(op_graph_ops_dict))

    def genesVariation(self, community):
        self.gene_fused_info = sorted(self.gene_fused_info, key=lambda item: item[1]['total_cost'], reverse=True)
        # 当前群落的基因图
        op_graph = community.graph
        op_graph_ops_dict = op_graph.nodes()._nodes
        # 选择一个基因对
        while True:
            # random tensor_size_firse compute_cost_first
            op_pair_size = len(self.gene_fused_info) - 1
            if self.gene_select_strategy == "random":
                target_op_pair = random.randint(0, op_pair_size)
            elif self.gene_select_strategy == "communicate_cost_firse":
                r = random.random()
                p = 0.8
                if r < p:
                    target_op_pair = random.randint(0, int((1-p) * op_pair_size))
                else:
                    target_op_pair = random.randint(int(p * op_pair_size), op_pair_size)
            elif self.gene_select_strategy == "compute_cost_first":
                # compute_cost_first
                target_op_pair = 1
                print("compute_cost_first")
            else:
                # fused_cost_first
                r = random.random()
                p = 0.8
                if r < p:
                    target_op_pair = random.randint(0, int((1 - p) * op_pair_size))
                else:
                    target_op_pair = random.randint(int(p * op_pair_size), op_pair_size)


            pair_key = self.gene_fused_info[target_op_pair][0]
            # 只改变没改变过的基因
            ops_pair = pair_key.split('_')
            from_ops = int(ops_pair[0])
            to_ops = int(ops_pair[1])
            # 如果这两个算子是一组的 则没必要放 他们只能一起放
            if self.op_fused:
                if op_graph_ops_dict[from_ops]['colocation_group'] != op_graph_ops_dict[to_ops]['colocation_group']:
                    # (1 / 2) * (1 + math.cos(((1 - 1 / (i + 1)) * math.pi)))
                    # 利用动态衰减函数，衰减cost
                    # _LOGGER.info("choose: " + pair_key)
                    # _LOGGER.info(self.gene_fused_info[target_op_pair][1]['total_cost'])
                    self.gene_fused_info[target_op_pair][1]['total_cost'] = \
                        (self.gene_fused_info[target_op_pair][1]['communicate_cost'] + self.gene_fused_info[target_op_pair][1]['compute_cost'])\
                        * (1 / 2) * (1 + math.cos(((1 - 1 / (self.gene_fused_info[target_op_pair][1]['choosed_times'] + 1)) * math.pi)))
                    self.gene_fused_info[target_op_pair][1]['choosed_times'] += 1
                    # _LOGGER.info(self.gene_fused_info[target_op_pair][1]['total_cost'])
                    # exit(1)
                    break
            else:
                break

        # 表示该基因存活
        community.survive_genes[pair_key] = 1
        survive_genes = copy.deepcopy(community.survive_genes)
        new_graph = copy.deepcopy(op_graph)
        new_genetic_graph = GeneticGraph(new_graph, self.getGraphId(), survive_genes, self.device_num)

        # import sys
        if op_graph_ops_dict[from_ops]['p'] == op_graph_ops_dict[to_ops]['p']:
            # 如果这个基因上的算子已经在一起了，则分开放
            if self.target_device_strategy == "offload_first":
                # 最小负载优先（设备选择策略）
                target_device1 = -1
                target_device2 = -1
                min_memory = 999999999999
                for i in range(len(community.device_memory)):
                    if community.device_memory[i] < min_memory:
                        min_memory = community.device_memory[i]
                        target_device1 = i
                min_memory = 999999999999
                for i in range(len(community.device_memory)):
                    if community.device_memory[i] < min_memory and i != target_device1:
                        min_memory = community.device_memory[i]
                        target_device2 = i
            else:
                # 随机选择（设备选择策略）
                target_device1, target_device2 = random.sample(self.device_index, 2)
            self.change_graph_placement(new_genetic_graph, from_ops, target_device1)
            self.change_graph_placement(new_genetic_graph, to_ops, target_device2)
        else:
            # 如果这个基因上的算子是分开放的，则放一起
            if self.target_device_strategy == "offload_first":
                # 负载最小优先
                target_device = -1
                min_memory = 999999999999
                for i in range(len(community.device_memory)):
                    if community.device_memory[i] < min_memory:
                        target_device = i
                        min_memory = community.device_memory[i]
            else:
                # 随机选择
                target_device = random.randint(0, self.device_num - 1)
            self.change_graph_placement(new_genetic_graph, from_ops, target_device)
            self.change_graph_placement(new_genetic_graph, to_ops, target_device)

        return new_genetic_graph


    def genesSwap(self, community_left, community_right):
        left_survive_genes = community_left.survive_genes
        right_survive_genes = community_right.survive_genes
        #
        community_left_ops_dict = community_left.graph.nodes()._nodes
        community_right_ops_dict = community_right.graph.nodes()._nodes

        new_left_community = GeneticGraph(copy.deepcopy(community_left.graph), self.getGraphId(), survive_genes=None,
                                          device_num=self.device_num)
        new_right_community = GeneticGraph(copy.deepcopy(community_right.graph), self.getGraphId(), survive_genes=None,
                                           device_num=self.device_num)
        # 将community_left的优质基因给new_right_community
        for pair_key, survive_time in left_survive_genes.items():
            ops_pair = pair_key.split('_')
            from_ops = int(ops_pair[0])
            to_ops = int(ops_pair[1])
            self.change_graph_placement(new_right_community, from_ops,
                                        community_left_ops_dict[from_ops]['p'])
            self.change_graph_placement(new_right_community, to_ops,
                                        community_left_ops_dict[to_ops]['p'])

        # 将community_right_ops_dict的优质基因给new_left_community
        for pair_key, survive_time in right_survive_genes.items():
            ops_pair = pair_key.split('_')
            from_ops = int(ops_pair[0])
            to_ops = int(ops_pair[1])
            self.change_graph_placement(new_left_community, from_ops,
                                        community_right_ops_dict[from_ops]['p'])
            self.change_graph_placement(new_left_community, to_ops,
                                        community_right_ops_dict[to_ops]['p'])

        # 把存活基因对清空
        community_left.survive_genes = {}
        community_right.survive_genes = {}

        return new_left_community, new_right_community

    def getGraphId(self):
        self.graph_id += 1
        return self.graph_id

    def getSurviveCommunity(self, community1, community2, community3=None):
        if community3 is None:
            if community1.run_time <= community2.run_time:
                return community1
            else:
                return community2
        else:
            if community1.run_time <= community2.run_time:
                survive_community = community1
            else:
                survive_community = community2
            if survive_community.run_time <= community3.run_time:
                return survive_community
            return community3

    def evolutionOfCommunity(self):
        evolution_start = time.time()
        max_iterations = FLAGS.max_iterations
        iterations = 0

        self.community_left.run_time = self.caculate_run_time(self.community_left)
        self.community_right.run_time = self.caculate_run_time(self.community_right)

        best_left_id = self.community_left.graph_id
        best_right_id = self.community_right.graph_id
        same_time_left = 0
        same_time_right = 0
        max_same_time = 100
        while True:
            _LOGGER.info("----------------------------------")
            iter_start = time.time()
            # 基因交换和基因突变以一定频率交叉进行
            if (iterations + 1) % 5 == 0:
            # if iterations == -1:
                # 两个群落交换优质基因
                _LOGGER.info("genesSwap ...... ")
                new_left_community, new_right_community = self.genesSwap(self.community_left, self.community_right)
                # 计算时间
                new_left_community.run_time = self.caculate_run_time(new_left_community)
                new_right_community.run_time = self.caculate_run_time(new_right_community)

                _LOGGER.info("left community  id %d , run_time %d ", self.community_left.graph_id, self.community_left.run_time)
                _LOGGER.info("left swap       id %d , run_time %d ", new_left_community.graph_id, new_left_community.run_time)
                _LOGGER.info("right community id %d , run_time %d ", self.community_right.graph_id, self.community_right.run_time)
                _LOGGER.info("right swap      id %d , run_time %d ", new_right_community.graph_id, new_right_community.run_time)

                # 计算存活的群落
                # if new_left_community.is_OOM is False:
                self.community_left = self.getSurviveCommunity(self.community_left, new_left_community)
                # if new_right_community.is_OOM is False:
                self.community_right = self.getSurviveCommunity(self.community_right, new_right_community)
            else:
                _LOGGER.info("genesVariation ...... ")
                # 两个竞争群落（放置策略）各自变异
                # communityLeft变异
                left_variation = self.genesVariation(self.community_left)
                left_variation.run_time = self.caculate_run_time(left_variation)

                # communityRight变异
                right_variation = self.genesVariation(self.community_right)
                right_variation.run_time = self.caculate_run_time(right_variation)
                # 一共四个种群，利用多维代价评估模型 计算所有种群的代价
                _LOGGER.info("left community  id %d , run_time %d ", self.community_left.graph_id,
                             self.community_left.run_time)
                _LOGGER.info("left variation   id %d , run_time %d ", left_variation.graph_id,
                             left_variation.run_time)
                _LOGGER.info("right community id %d , run_time %d ", self.community_right.graph_id,
                             self.community_right.run_time)
                _LOGGER.info("right variation  id %d , run_time %d ", right_variation.graph_id,
                             right_variation.run_time)

                # 保留更优秀的种群，作为下一轮迭代的起始状态
                # 取run_time最小的作为新一轮变异的群落
                # if left_together_variation.is_OOM is False or left_split_variation.is_OOM is False:
                self.community_left = self.getSurviveCommunity(self.community_left, left_variation)
                # if right_together_variation.is_OOM is False or right_split_variation.is_OOM is False:
                # 取run_time最小的作为新一轮变异的群落
                self.community_right = self.getSurviveCommunity(self.community_right, right_variation)

            # _LOGGER.info("memory using .... ")
            # _LOGGER.info("left community : %s, %lf", str(self.community_left.device_memory), self.community_left.balence_factor)
            # _LOGGER.info("right community : %s, %lf", str(self.community_right.device_memory), self.community_right.balence_factor)

            iter_end = time.time()
            iterations = iterations + 1

            _LOGGER.info("----------------------------------")
            _LOGGER.info("iterations: %d / %d, spend time %d s",
                         iterations, max_iterations, iter_end - iter_start)
            # ---判断是否达到终止条件-------
            if iterations == max_iterations:
                break

            if self.community_left.graph_id == best_left_id:
                if same_time_left == max_same_time:
                    break
                same_time_left += 1
            else:
                best_left_id = self.community_left.graph_id
                same_time_left = 0

            if self.community_right.graph_id == best_right_id:
                if same_time_right == max_same_time:
                    break
                same_time_right += 1
            else:
                best_right_id = self.community_right.graph_id
                same_time_right = 0
            # -----------------------------

            _LOGGER.info("left community same time: %d / %d", same_time_left, max_same_time)
            _LOGGER.info("right community same time: %d / %d", same_time_right, max_same_time)

        if self.community_left.is_OOM and self.community_right.is_OOM:
            _LOGGER.info("note: both community will happen OOM, stop searching!")
            # exit(1)

        evolution_end = time.time()
        _LOGGER.info("evolution total spend %d s, The number of iterations: %d",
                     evolution_end - evolution_start, iterations)


    def get_op_memory(self, op_data):
        return op_data["persistent_memory"] + sum(op_data['output_memory'])

    # 为群落指定分配设备
    def change_graph_placement(self, genetic_graph, target_op, target_device):
        # 指定一个新目标设备
        community_op_graph_ops_dict = genetic_graph.graph.nodes()._nodes
        community_op_graph_ops_dict[target_op]['p'] = target_device
        # 将同组的设备一起调度
        #if 'colocation_group' in community_op_graph_ops_dict[target_op]:
        if self.op_fused:
            co_group = community_op_graph_ops_dict[target_op]['colocation_group']
            if len(self.colocation_group[co_group]['ops']) > 1:
                for group_member_ops in self.colocation_group[co_group]['ops']:
                    ops_id = group_member_ops['id']
                    # 指定新目标设备
                    community_op_graph_ops_dict[ops_id]['p'] = target_device

    # 初始化群落
    def init_graph_community(self, op_graph):
        if self.init_fun is not None:
            initial_graph = self.init_fun(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))
            self.community_left = GeneticGraph(copy.deepcopy(initial_graph), self.getGraphId(), device_num=self.device_num)
            self.community_right = GeneticGraph(copy.deepcopy(initial_graph), self.getGraphId(), device_num=self.device_num)
        else:
            if FLAGS.placement_method == "random":
                self.community_left = GeneticGraph(copy.deepcopy(op_graph), self.getGraphId(), device_num=self.device_num)
                self.initial_graph_placement(self.community_left, "random_gpu")

                self.community_right = GeneticGraph(copy.deepcopy(op_graph), self.getGraphId(), device_num=self.device_num)
                self.initial_graph_placement(self.community_right, "random_gpu")
            elif FLAGS.placement_method == "specified":

                initial_graph = nx.read_gpickle("/root/heuristic-search/graph_gpickles/initial_graph.gpickle")
                # 一个为指定策略，一个为随机
                self.community_left = GeneticGraph(copy.deepcopy(initial_graph), self.getGraphId(), device_num=self.device_num)

                self.community_right = GeneticGraph(copy.deepcopy(op_graph), self.getGraphId(),
                                                   device_num=self.device_num)
                self.initial_graph_placement(self.community_right, "random_gpu")

                # self.community_right = GeneticGraph(copy.deepcopy(initial_graph), self.getGraphId(), device_num=self.device_num)

        _LOGGER.info("init graph community finished")

    def caculateTotalOutputMemory(self, graph):
        totalOutputMemory = 0
        op_graph_ops_dict = graph.nodes()._nodes
        for parent_id, parent_node in op_graph_ops_dict.items():
            descend_ids = graph.successors(parent_id)
            for succ_id in descend_ids:
                if len(parent_node['output_memory']) > 0:
                    if op_graph_ops_dict[succ_id]['p'] != parent_node['p']:
                        totalOutputMemory += sum(parent_node['output_memory'])
        return totalOutputMemory

    def calculate_balence_factor(self, graph):
        device_memory = []
        for i in range(self.device_num):
            device_memory.append(0)

        total_need_memory = 0
        # 计算各个设备的内存量
        op_graph_ops_dict = graph.nodes()._nodes
        for ops_id, node in op_graph_ops_dict.items():
            op_memory = self.get_op_memory(node)
            device_memory[node['p']] += op_memory
            total_need_memory += op_memory

        memory_per_device = total_need_memory / self.device_num
        # 计算负载均衡系数
        balence_factor = 0
        for memory in device_memory:
            balence_factor += abs(memory - memory_per_device) / memory_per_device

        return balence_factor, device_memory

    def calculate_balence_factor2(self, memory_used):

        total_need_memory = 0
        for memory in memory_used:
            total_need_memory += memory

        memory_per_device = total_need_memory / self.device_num
        # 计算负载均衡系数
        balence_factor = 0
        for memory in memory_used:
            balence_factor += abs(memory - memory_per_device) / memory_per_device

        return balence_factor

    # 多维代价评估模型 评估策略的优劣（run_time）
    def caculate_run_time(self, community):
        graph = community.graph
        ungrouped_pl = {}
        op_graph_ops_dict = graph.nodes()._nodes

        for op in self.mg.node:
            ungrouped_pl[op.name] = 0

        for id, data in op_graph_ops_dict.items():
            ungrouped_pl[data['name']] = data['p']

        for op_name, device in ungrouped_pl.items():
            if device == -1:
                ungrouped_pl[op_name] = 0
                for id, data in op_graph_ops_dict.items():
                    if data['name'] == op_name:
                        data['p'] = 0
                        break
        run_time, memory_used = self.simulator.simulate(ungrouped_pl, sim_mem_usage=True)

        # 使用评估的内存
        balence_factor = self.calculate_balence_factor2(memory_used)
        community.balence_factor = balence_factor
        community.device_memory = memory_used

        memory = self.caculateTotalOutputMemory(graph)
        if not self.offload_balence_mode:
            balence_factor = 0

        comfort = run_time + int(self.comm_cost_fn(memory) * self.LAMBDA) * (1 + balence_factor)
        return comfort

    # 这个是baechi自带的模拟器
    def caculate_run_time1(self, graph):
        device_graph = copy.deepcopy(self.device_graph)
        scheduler = VirtualScheduler(graph, device_graph)
        scheduler.initialize()
        run_time = scheduler.run()
        return run_time

    # placeto的模拟器 评估策略的优劣（run_time）
    def caculate_run_time2(self, community):
        graph = community.graph
        ungrouped_pl = {}
        op_graph_ops_dict = graph.nodes()._nodes

        for op in self.mg.node:
            ungrouped_pl[op.name] = 0

        for id, data in op_graph_ops_dict.items():
            ungrouped_pl[data['name']] = data['p']

        for op_name, device in ungrouped_pl.items():
            if device == -1:
                ungrouped_pl[op_name] = 0
                for id, data in op_graph_ops_dict.items():
                    if data['name'] == op_name:
                        data['p'] = 0
                        break
        run_time, memory_used = self.simulator.simulate(ungrouped_pl, sim_mem_usage=True)

        return run_time


    # 根据ratio调整op的内存大小（有些batch大小单卡上profile不出来 就用这个办法来做）
    # 当batchsize太大 单机难以profile出cost时，先将bs调小，然后用这个ratio模拟大batch size下的tensor内存大小
    def repair_op_memory(self, op_graph, memory_ratio):
        for op_id, node in op_graph.nodes()._nodes.items():
            node['output_memory'] *= memory_ratio
            node['temporary_memory'] *= memory_ratio

    def fun(self, op_graph):
        dict = op_graph.nodes()._nodes

        _LOGGER.info("initi num %d ", len(dict))

        #print(self.colocation_group)
        MAX = -1
        MAX_MEMORY = 0
        MIN = 9999
        MIN_MEMORY = 99999999999999
        for group, v in self.colocation_group.items():
            #print(v)
            # group = v['colocation_group']
            #_LOGGER.info("group: %s, value:%d", group, len(v['ops']))
            if len(v['ops']) > MAX:
                MAX = len(v['ops'])
            if len(v['ops']) < MIN:
                MIN = len(v['ops'])

            memory = 0
            memory = memory + v['persistent_memory_sum'] + v['output_memory_sum']

            if MAX_MEMORY < memory:
                MAX_MEMORY = memory
            if MIN_MEMORY > memory:
                MIN_MEMORY = memory

        _LOGGER.info("group num: %d ", len(self.colocation_group))
        _LOGGER.info("MAX: %d , MIN: %d", MAX, MIN)
        _LOGGER.info("MAX_memory: %d , MIN_memory: %d", MAX_MEMORY, MIN_MEMORY)


        # for k, v in d.items():
        #     _LOGGER.info("key: %s, value:%d", k, v)
        # number
        # of
        # nodes
        # 18089
        # number
        # of
        # edges
        # 32892
        exit(1)


    def __init__(self, op_graph, device_graph, cost_dict, comm_cost_fn, init_fun=None):

        self.LAMBDA = FLAGS.LAMBDA
        self.offload_balence_mode = FLAGS.offload_balence_mode
        # self.target_device_strategy = "offload_first"
        self.target_device_strategy = "random"

        self.gene_select_strategy = "fused_cost_first" # random communicate_cost_firse compute_cost_first fused_cost_first

        # 修正相关内存
        self.repair_op_memory(op_graph, memory_ratio=FLAGS.memory_ratio)
        self.comm_cost_fn = comm_cost_fn
        self.init_fun = init_fun

        self.graph_nodes_num = len(op_graph.nodes()._nodes)

        self.colocation_group = placer_utils.create_colocation_group_infos(op_graph)

        self.device_graph = device_graph
        self.device_graph_dict = device_graph.nodes()._nodes
        self.device_num = len(self.device_graph_dict)
        self.device_index = range(self.device_num)

        self.device_names = ['/device:GPU:%d' % i for i in range(self.device_num)]
        self.cost_dict = cost_dict
        self.mg = self.cost_dict['graphdef']
        self.op_perf = self.cost_dict['op_perf']
        self.step_stats = self.cost_dict['step_stats']
        # 迭代的群落
        self.community_left = None
        self.community_right = None

        # 是否使用算子融合
        self.op_fused = True

        # 执行模拟器
        self.simulator = ImportantOpsSimulator(self.mg, self.op_perf, self.step_stats, self.device_names)
        # 获取基因上的相关信息
        self.MAX_GENE_TENSOR_SIZE = 0
        self.MAX_GENE_COMPUTE_COST = 0


        self.gene_fused_info = {}
        op_graph_ops_dict = op_graph.nodes()._nodes
        i = 0
        for parent_id, parent_node in op_graph_ops_dict.items():
            descend_ids = op_graph.successors(parent_id)
            for succ_id in descend_ids:
                if op_graph_ops_dict[parent_id]['colocation_group'] == op_graph_ops_dict[succ_id]['colocation_group']:
                    i += 1
                    continue

                pair = str(parent_id) + "_" + str(succ_id)
                d = {}
                # 通信代价
                d['communicate_cost'] = self.comm_cost_fn(sum(parent_node['output_memory']))
                # 基因上两个算子的计算代价之和
                d['compute_cost'] = parent_node['weight'] + op_graph_ops_dict[succ_id]['weight']
                # 被选择中的次数
                d['choosed_times'] = 1
                # 总代价 = (通信代价 + 计算代价) * (次数 / 最大次数)
                d['total_cost'] = (d['communicate_cost'] + d['compute_cost']) * (1 / 1)
                self.gene_fused_info[pair] = d

                # 记录最大值
                if self.MAX_GENE_TENSOR_SIZE < d['communicate_cost']:
                    self.MAX_GENE_TENSOR_SIZE = d['communicate_cost']
                if self.MAX_GENE_COMPUTE_COST < d['compute_cost']:
                    self.MAX_GENE_COMPUTE_COST = d['compute_cost']

        #         print(d)
        # print(self.MAX_GENE_TENSOR_SIZE)
        # print(self.MAX_GENE_COMPUTE_COST)
        #
        # print(len(self.colocation_group))
        # print(i)
        # print(len(self.gene_fused_info[pair]))
        # exit(1)

        # 将字典转化为list
        self.gene_fused_info = sorted(self.gene_fused_info.items(), key=lambda item: item[1]['total_cost'], reverse=True)

        self.graph_id = 0
        # 初始化
        self.init_graph_community(op_graph=op_graph)

        self.graph_dict = {}

        #self.fun(op_graph)

