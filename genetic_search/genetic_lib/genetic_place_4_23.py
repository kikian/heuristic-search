import copy
import random
from utils import logger
from placer import placer_utils
import time
from sim.tf_placement_sim.tf_pl_simulator import ImportantOpsSimulator
from genetic_lib.genetic_graph import GeneticGraph
from placer.virtual_scheduler import VirtualScheduler
import networkx as nx


_LOGGER = logger.get_logger(__file__, level=logger.INFO)

class Genetic_placer:

    # 为群落随机分配设备
    def initial_graph_placement(self, genetic_graph, init_type):
        assert init_type in ["random_gpu", "single_gpu"]

        op_graph = genetic_graph.graph

        ops_count = 0
        op_graph_ops_dict = op_graph.nodes()._nodes
        for ops_id, data in op_graph_ops_dict.items():
            # 如果没有放置过
            if 'p' not in data:
                # 防止初始化出来的策略就出现了OOM，虽然概率极低
                #while True:
                if init_type == "random_gpu":
                    target_device = random.randint(0, self.device_num - 1)
                elif init_type == "single_gpu":
                    target_device = 0
                # 随机指定一个目标设备
                self.change_graph_placement(genetic_graph, ops_id, target_device)
            ops_count = ops_count + 1

        _LOGGER.info("initial finished total: %d / %d", ops_count, len(op_graph_ops_dict))

    def genesVariation(self, community):
        # 当前群落的基因图
        op_graph = community.graph
        op_graph_ops_dict = op_graph.nodes()._nodes
        # 随机选择一个基因对
        while True:
            target_op_pair = random.randint(0, len(self.ops_transfer_tensor) - 1)
            pair_key = self.ops_transfer_tensor[target_op_pair][0]
            # 只改变没改变过的基因
            ops_pair = pair_key.split('_')
            from_ops = int(ops_pair[0])
            to_ops = int(ops_pair[1])
            # 如果这两个算子是一组的 则没必要放 他们只能一起放
            if self.op_fused:
                if op_graph_ops_dict[from_ops]['colocation_group'] != op_graph_ops_dict[to_ops]['colocation_group']:
                    break
            else:
                break

        # 表示该基因存活
        community.survive_genes[pair_key] = 1
        survive_genes_together = copy.deepcopy(community.survive_genes)
        survive_genes_split = copy.deepcopy(community.survive_genes)

        new_graph_together = copy.deepcopy(op_graph)
        new_graph_split = copy.deepcopy(op_graph)

        genetic_graph_together = GeneticGraph(new_graph_together, self.getGraphId(), survive_genes_together, self.device_num)
        genetic_graph_split = GeneticGraph(new_graph_split, self.getGraphId(), survive_genes_split, self.device_num)

        # 1. 将这个基因对放一起（from_ops和to_ops）
        # while True:
        # 随机选一个 如果发生OOM则重新 直到找到可以放置的
        target_device = random.randint(0, self.device_num-1)

        self.change_graph_placement(genetic_graph_together, from_ops, target_device)
        self.change_graph_placement(genetic_graph_together, to_ops, target_device)


        # 2. 将这个基因对分开放（from_ops和to_ops）
        # 随机选两个
        target_device1, target_device2 = random.sample(self.device_index, 2)
        self.change_graph_placement(genetic_graph_split, from_ops, target_device1)
        self.change_graph_placement(genetic_graph_split, to_ops, target_device2)

        return genetic_graph_together, genetic_graph_split

    def genesSwap(self, community_left, community_right):
        left_survive_genes = community_left.survive_genes
        right_survive_genes = community_right.survive_genes
        #
        community_left_ops_dict = community_left.graph.nodes()._nodes
        community_right_ops_dict = community_right.graph.nodes()._nodes

        new_left_community = GeneticGraph(copy.deepcopy(community_left.graph), self.getGraphId(), survive_genes=None,
                                          device_num=self.device_num)
        new_right_community = GeneticGraph(copy.deepcopy(community_right.graph), self.getGraphId(), survive_genes=None,
                                           device_num=self.device_num)
        # 将community_left的优质基因给new_right_community
        for pair_key, survive_time in left_survive_genes.items():
            ops_pair = pair_key.split('_')
            from_ops = int(ops_pair[0])
            to_ops = int(ops_pair[1])
            self.change_graph_placement(new_right_community, from_ops,
                                        community_left_ops_dict[from_ops]['p'])
            self.change_graph_placement(new_right_community, to_ops,
                                        community_left_ops_dict[to_ops]['p'])

        # 将community_right_ops_dict的优质基因给new_left_community
        for pair_key, survive_time in right_survive_genes.items():
            ops_pair = pair_key.split('_')
            from_ops = int(ops_pair[0])
            to_ops = int(ops_pair[1])
            self.change_graph_placement(new_left_community, from_ops,
                                        community_right_ops_dict[from_ops]['p'])
            self.change_graph_placement(new_left_community, to_ops,
                                        community_right_ops_dict[to_ops]['p'])

        # 把存活基因对清空
        community_left.survive_genes = {}
        community_right.survive_genes = {}

        return new_left_community, new_right_community

    def getGraphId(self):
        self.graph_id += 1
        return self.graph_id

    def getSurviveCommunity(self, community1, community2, community3=None):
        if community3 is None:
            if community1.run_time <= community2.run_time:
                return community1
            else:
                return community2
        else:
            if community1.run_time <= community2.run_time:
                survive_community = community1
            else:
                survive_community = community2
            if survive_community.run_time <= community3.run_time:
                return survive_community
            return community3

    def evolutionOfCommunity(self):
        evolution_start = time.time()
        max_iterations = 100
        iterations = 0

        self.community_left.run_time = self.caculate_run_time(self.community_left)
        self.community_right.run_time = self.caculate_run_time(self.community_right)

        best_left_id = self.community_left.graph_id
        best_right_id = self.community_right.graph_id
        same_time_left = 0
        same_time_right = 0
        max_same_time = 100
        while True:
            _LOGGER.info("----------------------------------")
            iter_start = time.time()
            if (iterations + 1) % 5 == 0:
            # if iterations == -1:
                # 两个群落交换优质基因
                _LOGGER.info("genesSwap ...... ")
                new_left_community, new_right_community = self.genesSwap(self.community_left, self.community_right)
                # 计算时间
                new_left_community.run_time = self.caculate_run_time(new_left_community)
                new_right_community.run_time = self.caculate_run_time(new_right_community)

                _LOGGER.info("left community  id %d , run_time %d ", self.community_left.graph_id, self.community_left.run_time)
                _LOGGER.info("left swap       id %d , run_time %d ", new_left_community.graph_id, new_left_community.run_time)
                _LOGGER.info("right community id %d , run_time %d ", self.community_right.graph_id, self.community_right.run_time)
                _LOGGER.info("right swap      id %d , run_time %d ", new_right_community.graph_id, new_right_community.run_time)

                # 计算存活的群落
                # if new_left_community.is_OOM is False:
                self.community_left = self.getSurviveCommunity(self.community_left, new_left_community)
                # if new_right_community.is_OOM is False:
                self.community_right = self.getSurviveCommunity(self.community_right, new_right_community)
            else:
                _LOGGER.info("genesVariation ...... ")
                # 两个竞争群落各自变异
                # communityLeft变异
                left_together_variation, left_split_variation = self.genesVariation(self.community_left)
                left_together_variation.run_time = self.caculate_run_time(left_together_variation)
                left_split_variation.run_time = self.caculate_run_time(left_split_variation)

                # communityRight变异
                right_together_variation, right_split_variation = self.genesVariation(self.community_right)
                right_together_variation.run_time = self.caculate_run_time(right_together_variation)
                right_split_variation.run_time = self.caculate_run_time(right_split_variation)

                _LOGGER.info("left community  id %d , run_time %d ", self.community_left.graph_id,
                             self.community_left.run_time)
                _LOGGER.info("left together   id %d , run_time %d ", left_together_variation.graph_id,
                             left_together_variation.run_time)
                _LOGGER.info("left split      id %d , run_time %d ", left_split_variation.graph_id,
                             left_split_variation.run_time)
                _LOGGER.info("right community id %d , run_time %d ", self.community_right.graph_id,
                             self.community_right.run_time)
                _LOGGER.info("right together  id %d , run_time %d ", right_together_variation.graph_id,
                             right_together_variation.run_time)
                _LOGGER.info("right split     id %d , run_time %d ", right_split_variation.graph_id,
                             right_split_variation.run_time)

                # 取run_time最小的作为新一轮变异的群落
                # if left_together_variation.is_OOM is False or left_split_variation.is_OOM is False:
                self.community_left = self.getSurviveCommunity(self.community_left, left_together_variation,
                                                               left_split_variation)
                # if right_together_variation.is_OOM is False or right_split_variation.is_OOM is False:
                    # 取run_time最小的作为新一轮变异的群落
                self.community_right = self.getSurviveCommunity(self.community_right, right_together_variation,
                                                                right_split_variation)

            _LOGGER.info("memory using .... ")
            _LOGGER.info("left community : %s, %lf", str(self.community_left.device_memory), self.community_left.balence_factor)
            _LOGGER.info("right community : %s, %lf", str(self.community_right.device_memory), self.community_right.balence_factor)

            iter_end = time.time()
            iterations = iterations + 1

            _LOGGER.info("----------------------------------")
            _LOGGER.info("iterations: %d / %d, spend time %d s",
                         iterations, max_iterations, iter_end - iter_start)

            if iterations == max_iterations:
                break

            if self.community_left.graph_id == best_left_id:
                if same_time_left == max_same_time:
                    break
                same_time_left += 1
            else:
                best_left_id = self.community_left.graph_id
                same_time_left = 0

            if self.community_right.graph_id == best_right_id:
                if same_time_right == max_same_time:
                    break
                same_time_right += 1
            else:
                best_right_id = self.community_right.graph_id
                same_time_right = 0

            _LOGGER.info("left community same time: %d / %d", same_time_left, max_same_time)
            _LOGGER.info("right community same time: %d / %d", same_time_right, max_same_time)

        if self.community_left.is_OOM and self.community_right.is_OOM:
            _LOGGER.info("note: both community will happen OOM, stop searching!")
            # exit(1)

        evolution_end = time.time()
        _LOGGER.info("evolution total spend %d s, The number of iterations: %d",
                     evolution_end - evolution_start, iterations)

    def get_op_memory(self, op_data):
        return op_data["persistent_memory"] + sum(op_data['output_memory'])

    # 为群落指定分配设备
    def change_graph_placement(self, genetic_graph, target_op, target_device):
        # 指定一个新目标设备
        community_op_graph_ops_dict = genetic_graph.graph.nodes()._nodes
        community_op_graph_ops_dict[target_op]['p'] = target_device
        # 将同组的设备一起调度
        #if 'colocation_group' in community_op_graph_ops_dict[target_op]:
        if self.op_fused:
            co_group = community_op_graph_ops_dict[target_op]['colocation_group']
            if len(self.colocation_group[co_group]['ops']) > 1:
                for group_member_ops in self.colocation_group[co_group]['ops']:
                    ops_id = group_member_ops['id']
                    # 指定新目标设备
                    community_op_graph_ops_dict[ops_id]['p'] = target_device

    # 初始化群落
    def init_graph_community(self, op_graph):
        if self.init_fun is not None:
            initial_graph = self.init_fun(copy.deepcopy(op_graph), copy.deepcopy(self.device_graph))
            self.community_left = GeneticGraph(copy.deepcopy(initial_graph), self.getGraphId(), device_num=self.device_num)
            self.community_right = GeneticGraph(copy.deepcopy(initial_graph), self.getGraphId(), device_num=self.device_num)
        else:
            self.community_left = GeneticGraph(copy.deepcopy(op_graph), self.getGraphId(), device_num=self.device_num)
            self.initial_graph_placement(self.community_left, "random_gpu")

            self.community_right = GeneticGraph(copy.deepcopy(op_graph), self.getGraphId(), device_num=self.device_num)
            self.initial_graph_placement(self.community_right, "random_gpu")
        # else:
        #     initial_graph = nx.read_gpickle("./graph_gpickles/initial_graph.gpickle")
        #     self.community_left = GeneticGraph(copy.deepcopy(initial_graph), self.getGraphId(), device_num=self.device_num)
        #     self.community_right = GeneticGraph(copy.deepcopy(initial_graph), self.getGraphId(), device_num=self.device_num)

        _LOGGER.info("init graph community finished")

    def caculateTotalOutputMemory(self, graph):
        totalOutputMemory = 0
        op_graph_ops_dict = graph.nodes()._nodes
        for parent_id, parent_node in op_graph_ops_dict.items():
            descend_ids = graph.successors(parent_id)
            for succ_id in descend_ids:
                if len(parent_node['output_memory']) > 0:
                    if op_graph_ops_dict[succ_id]['p'] != parent_node['p']:
                        totalOutputMemory += sum(parent_node['output_memory'])
        return totalOutputMemory

    def calculate_balence_factor(self, graph):
        device_memory = []
        for i in range(self.device_num):
            device_memory.append(0)

        total_need_memory = 0
        # 计算各个设备的内存量
        op_graph_ops_dict = graph.nodes()._nodes
        for ops_id, node in op_graph_ops_dict.items():
            op_memory = self.get_op_memory(node)
            device_memory[node['p']] += op_memory
            total_need_memory += op_memory

        memory_per_device = total_need_memory / self.device_num
        # 计算负载均衡系数
        balence_factor = 0
        for memory in device_memory:
            balence_factor += abs(memory - memory_per_device) / memory_per_device

        return balence_factor, device_memory

    def calculate_balence_factor2(self, memory_used):

        total_need_memory = 0
        for memory in memory_used:
            total_need_memory += memory

        memory_per_device = total_need_memory / self.device_num
        # 计算负载均衡系数
        balence_factor = 0
        for memory in memory_used:
            balence_factor += abs(memory - memory_per_device) / memory_per_device

        return balence_factor


    # placeto的模拟器 + 线性拟合的通信代价 作为 run_time
    def caculate_run_time(self, community):
        graph = community.graph
        ungrouped_pl = {}
        op_graph_ops_dict = graph.nodes()._nodes

        for op in self.mg.node:
            ungrouped_pl[op.name] = 0

        for id, data in op_graph_ops_dict.items():
            ungrouped_pl[data['name']] = data['p']

        for op_name, device in ungrouped_pl.items():
            if device == -1:
                ungrouped_pl[op_name] = 0
                for id, data in op_graph_ops_dict.items():
                    if data['name'] == op_name:
                        data['p'] = 0
                        break
        run_time, memory_used = self.simulator.simulate(ungrouped_pl, sim_mem_usage=True)
        # run_time = self.simulator.simulate(ungrouped_pl, sim_mem_usage=False)
        # todo: 这里添加OOM检测
        # _LOGGER.info(run_time)
        # _LOGGER.info(memory_used)
        # exit(1)

        # balence_factor, device_memory = self.calculate_balence_factor(graph)

        # 使用placeto模拟器评估的内存计算
        balence_factor = self.calculate_balence_factor2(memory_used)
        community.balence_factor = balence_factor
        # community.device_memory = device_memory
        community.device_memory = memory_used

        memory = self.caculateTotalOutputMemory(graph)

        offload_balence_mode = False
        if offload_balence_mode:
            comfort = run_time + int(self.comm_cost_fn(memory) * 1) * (1 + balence_factor)
        else:
            comfort = run_time + int(self.comm_cost_fn(memory) * 1)

        return comfort
        #return run_time

    # 这个是baechi自带的模拟器
    def caculate_run_time1(self, graph):
        device_graph = copy.deepcopy(self.device_graph)
        scheduler = VirtualScheduler(graph, device_graph)
        scheduler.initialize()
        run_time = scheduler.run()
        return run_time

    # 根据ratio调整op的内存大小（有些batch大小单卡上profile不出来 就用这个办法来做）
    # 当batchsize太大 单机难以profile出cost时，先将bs调小，然后用这个ratio模拟大batch size下的tensor内存大小
    def repair_op_memory(self, op_graph, memory_ratio):
        for op_id, node in op_graph.nodes()._nodes.items():
            node['output_memory'] *= memory_ratio
            node['temporary_memory'] *= memory_ratio

    def __init__(self, op_graph, device_graph, cost_dict, comm_cost_fn, init_fun=None):
        # 修正相关内存
        self.repair_op_memory(op_graph, memory_ratio=8)
        self.comm_cost_fn = comm_cost_fn
        self.init_fun = init_fun

        self.graph_nodes_num = len(op_graph.nodes()._nodes)

        self.colocation_group = placer_utils.create_colocation_group_infos(op_graph)


        # 不同组的连边数量
        # map = {}
        # count = 0
        # op_graph_ops_dict = op_graph.nodes()._nodes
        # for parent_id, parent_node in op_graph_ops_dict.items():
        #     descend_ids = op_graph.successors(parent_id)
        #     for succ_id in descend_ids:
        #         if op_graph_ops_dict[succ_id]['colocation_group'] != parent_node['colocation_group']:
        #             count += 1
        #         map[op_graph_ops_dict[succ_id]['colocation_group']] = 1
        #         map[parent_node['colocation_group']] = 1
        #
        #         # if len(parent_node['output_memory']) > 0:
        #         #     if op_graph_ops_dict[succ_id]['p'] != parent_node['p']:
        #         #         totalOutputMemory += sum(parent_node['output_memory'])
        #
        # print(count)
        # print(len(map))
        # print(len(self.colocation_group))
        # exit(1)
        #
        # # for k, group in self.colocation_group.items():
        # #     if len(group['ops']) > 1:
        # #         print(k)
        # #         print(len(group['ops']))
        #
        # exit(1)


        # import networkx as nx
        # nx.write_gpickle(op_graph, "./graph_gpickles/op_graph.gpickle")

        self.device_graph = device_graph
        self.device_graph_dict = device_graph.nodes()._nodes
        self.device_num = len(self.device_graph_dict)
        self.device_index = range(self.device_num)

        self.device_names = ['/device:GPU:%d' % i for i in range(self.device_num)]
        self.cost_dict = cost_dict
        self.mg = self.cost_dict['graphdef']
        self.op_perf = self.cost_dict['op_perf']
        self.step_stats = self.cost_dict['step_stats']
        # 迭代的群落
        self.community_left = None
        self.community_right = None

        # 是否使用算子融合
        self.op_fused = True

        # 获取张量字典
        self.ops_transfer_tensor = {}
        # 执行模拟器
        self.simulator = ImportantOpsSimulator(self.mg, self.op_perf, self.step_stats, self.device_names)

        op_graph_ops_dict = op_graph.nodes()._nodes
        for parent_id, parent_node in op_graph_ops_dict.items():
            descend_ids = op_graph.successors(parent_id)
            for succ_id in descend_ids:
                pair = str(parent_id) + "_" + str(succ_id)
                self.ops_transfer_tensor[pair] = sum(parent_node['output_memory'])

        self.ops_transfer_tensor = sorted(self.ops_transfer_tensor.items(), key=lambda item: item[1], reverse=True)

        self.graph_id = 0
        # 初始化
        self.init_graph_community(op_graph=op_graph)

        self.graph_dict = {}

