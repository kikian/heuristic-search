# Copyright 2020 University of Illinois Board of Trustees. All Rights Reserved.
# Author: Beomyeol Jeon, DPRG (https://dprg.cs.uiuc.edu)
# This file is part of Baechi, which is released under specific terms. See file License.txt file for full license details.
# ==============================================================================
"""Virtual Scheduler."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from genetic_lib.device import DeviceWrapperAllocator
import copy

class OOMChecker():

    def __init__(self, op_graph, device_graph):
        self.op_graph = op_graph
        # self.device_graph = device_graph
        self.device_graph = copy.deepcopy(device_graph)

        self._devices = {
            device_id: DeviceWrapperAllocator(
                device_id, self.device_graph, self.op_graph, False)
            for device_id in self.device_graph.nodes}

        self.memory_limit = device_graph.nodes()._nodes[0]['memory_limit']

    def ckech_OOM(self):

        for op_id, op_data in self.op_graph.nodes().items():
            op_id = op_data['id']
            device_id = op_data['p']
            device = self._devices[device_id]
            device.run_op(op_id)

        for device_id, device in self._devices.items():
            if device.peak_memory > self.memory_limit:
                print(device.peak_memory)
                return True
        return False

