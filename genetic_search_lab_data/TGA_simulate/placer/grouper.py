# Copyright 2020 University of Illinois Board of Trustees. All Rights Reserved.
# Author: Beomyeol Jeon, DPRG (https://dprg.cs.uiuc.edu)
# This file is part of Baechi, which is released under specific terms. See file License.txt file for full license details.
# ==============================================================================
"""Grouper module."""
# pylint: disable=invalid-name
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import functools
import itertools
import tensorflow as tf

from placer import placer_utils as utils
from utils import logger

# from placer import placer_lib as plib
# from train import parse_comm_cost_coeffs

_LOGGER = logger.get_logger(__file__, level=logger.INFO)
FLAGS = tf.app.flags.FLAGS

tf.app.flags.DEFINE_enum('grouper', 'tf', ['tf', 'coplace', 'randomplace', 'costplace'],
                         'Grouping algorithm')


def get_comm_cost(tensor_size, coef, intercept):
    """Returns communication cost (in microsecs) for the given tensor size."""
    return int(round(coef * tensor_size + intercept))

def parse_comm_cost_coeffs(coeffs_str, factor=1.0):
    comm_cost_coeffs = coeffs_str.split(',')
    assert len(comm_cost_coeffs) == 2

    comm_cost_coeffs[0] = float(comm_cost_coeffs[0])
    comm_cost_coeffs[1] = int(comm_cost_coeffs[1])

    if factor != 1.0:
        _LOGGER.info('Communication cost factor: %s', str(factor))
        comm_cost_coeffs = tuple(
            [value * factor for value in comm_cost_coeffs])

    return comm_cost_coeffs


class Grouper(object):
    """Default grouper that does nothing."""

    def __call__(self, op_graph):
        raise NotImplementedError()


def _len_and_str(string):
    """Returns a tuple of string length and string."""
    return (len(string), string)


def _update_colocation_group(op_graph, colocation_group_map):
    """Updates colocation groups of operators in op_graph with new mapping."""
    # pick the shortest group name among groups as new group name
    group_dict = {group: min(group_set, key=_len_and_str)
                  for group, group_set in colocation_group_map.items()}

    # print merged groups
    reverse_mapping = {}
    for prev_name, new_name in group_dict.items():
        if new_name in reverse_mapping:
            reverse_mapping[new_name].append(prev_name)
        else:
            reverse_mapping[new_name] = [prev_name]
    for new_name, prev_names in reverse_mapping.items():
        _LOGGER.debug('Change group: %s -> %s', sorted(prev_names), new_name)

    # update colocation group
    for _, op_data in op_graph.nodes.items():
        if isinstance(op_data['colocation_group'], list):
            new_group = None
            for colocation_group in op_data['colocation_group']:
                ret = group_dict.get(colocation_group, colocation_group)
                if new_group is None:
                    new_group = ret
                else:
                    assert new_group == ret, 'node=%s, cur=%s, new=%s' % (
                        op_data['name'], new_group, ret)
        else:
            prev_group_name = op_data['colocation_group']
            new_group = group_dict.get(prev_group_name, prev_group_name)

        op_data['colocation_group'] = new_group


def process_colocation_group(op_graph):
    """Process a list of colocations groups into a single colocation group."""

    # This maps a colocation group name to a set of other group names
    colocation_group_map = utils.ColocationGroupMap()

    for _, op_data in op_graph.nodes.items():
        colocation_group = op_data['colocation_group']
        for op1, op2 in itertools.combinations(colocation_group, 2):
            colocation_group_map.colocate(op1, op2)

    _update_colocation_group(op_graph, colocation_group_map)


class TFColocationGrouper(Grouper):
    """Generate a new graph by using TensorFlow colocation group information."""

    def __call__(self, op_graph):
        # use the existing colocation group information
        process_colocation_group(op_graph)
        return op_graph


class CoplacementGrouper(Grouper):
    """Generate a new graph by using heuristic at the paper."""

    def __init__(self, log_colocation_group=False, ignore_control_edges=False):
        super(CoplacementGrouper, self).__init__()
        self._log_colocation_group = log_colocation_group
        self._ignore_control_edges = ignore_control_edges

    @staticmethod
    def _run_colocation_step(op_graph, ignore_control_edges):
        """Check whether there are operators that can be co-located.

        When the output of an operator is consumed only by another operator,
        assign the same colocation group for them

        将那些出度入度只为一个的算子的节点放在一起（一个group）

        Returns:
          True if there are opeartors that can be co-located.
          False, otherwise.
        """
        colocation_candidates = utils.ColocationGroupMap()

        for op_id, op_data in op_graph.nodes.items():
            # TODO: should we consider tensor-wise? not operator wise?
            out_edges = list(op_graph.out_edges(op_id))
            # 只考虑出度为1的节点
            if len(out_edges) != 1:
                continue

            next_op_id = out_edges[0][1]

            # pass control edges because this does not have data transfer
            edge_data = op_graph.get_edge_data(op_id, next_op_id)
            # 跳过那些没有数据传输的节点
            if ignore_control_edges and edge_data["is_control"]:
                continue

            next_op_data = op_graph.nodes[next_op_id]

            op_group = op_data['colocation_group']
            next_op_group = next_op_data['colocation_group']

            if op_group != next_op_group:
                # these two can be colocated
                _LOGGER.debug('Possible colocation ops. %s[%s] -> %s[%s]',
                              op_data['name'],
                              op_group,
                              next_op_data['name'],
                              next_op_group)
                colocation_candidates.colocate(op_group, next_op_group)

        if len(colocation_candidates) > 0:
            _update_colocation_group(op_graph, colocation_candidates)
            return True

        return False

    def __call__(self, op_graph):
        process_colocation_group(op_graph)
        # first use default colocation group information
        if self._log_colocation_group:
            with open('tf_colocation_groups.log', 'w') as f:
                utils.print_colocation_group(
                    op_graph, print_cb=lambda v: f.write(v + '\n'))
        while self._run_colocation_step(op_graph, self._ignore_control_edges):
            pass
        if self._log_colocation_group:
            with open('coplaced_groups.log', 'w') as f:
                utils.print_colocation_group(
                    op_graph, print_cb=lambda v: f.write(v + '\n'))
        return op_graph


class RandomplacementGrouper(Grouper):
    """Generate a new graph by using heuristic at the paper."""

    def __init__(self, log_colocation_group=False, ignore_control_edges=False):
        super(RandomplacementGrouper, self).__init__()
        self._log_colocation_group = log_colocation_group
        self._ignore_control_edges = ignore_control_edges

    @staticmethod
    def _run_colocation_step(op_graph, ignore_control_edges):
        """
            随机分组
        """
        colocation_candidates = utils.ColocationGroupMap()

        colocation_group_name = set()

        for op_id, op_data in op_graph.nodes.items():
            colocation_group_name.add(op_data['colocation_group'])

        name_lst = []
        for name in colocation_group_name:
            name_lst.append(name)

        import random
        for op_id, op_data in op_graph.nodes.items():
            # index = random.randint(0, len(name_lst) - 1)
            # op_data['colocation_group'] = name_lst[index]
            # op_data['colocation_group'] = name_lst[0]
            op_data['colocation_group'] = str(random.randint(0,3000))

        if len(colocation_candidates) > 0:
            _update_colocation_group(op_graph, colocation_candidates)
            return True

        return False

    def __call__(self, op_graph):
        process_colocation_group(op_graph)
        # first use default colocation group information
        if self._log_colocation_group:
            with open('tf_colocation_groups.log', 'w') as f:
                utils.print_colocation_group(
                    op_graph, print_cb=lambda v: f.write(v + '\n'))
        while self._run_colocation_step(op_graph, self._ignore_control_edges):
            pass
        if self._log_colocation_group:
            with open('coplaced_groups.log', 'w') as f:
                utils.print_colocation_group(
                    op_graph, print_cb=lambda v: f.write(v + '\n'))
        return op_graph

class CostplacementGrouper(Grouper):
    """Generate a new graph by using heuristic at the paper."""

    def __init__(self, log_colocation_group=False, ignore_control_edges=False):
        super(CostplacementGrouper, self).__init__()
        self._log_colocation_group = log_colocation_group
        self._ignore_control_edges = ignore_control_edges

    @staticmethod
    def _run_colocation_step(op_graph, ignore_control_edges):
        """
            将计算代价小于通信代价的节点分为一组
        """
        # 通信代价计算
        comm_cost_coeffs = parse_comm_cost_coeffs(
            FLAGS.comm_cost_coeffs, FLAGS.comm_cost_factor)
        comm_cost_coef, comm_cost_intercept = comm_cost_coeffs
        # get_comm_cost根据公式计算开销
        comm_cost_fn = functools.partial(get_comm_cost,
                                         coef=comm_cost_coef,
                                         intercept=comm_cost_intercept)

        # before
        d = {}
        for op_id, op_data in op_graph.nodes.items():
            group = op_data['colocation_group']
            if not d.__contains__(group):
                d[group] = 1
            else:
                d[group] += 1

        for k in d.keys():
            print(str(k) + " : " + str(d[k]))
        print(len(d))
        # exit(1)

        # op_graph_ops_dict = op_graph.nodes()._nodes
        # for op_id, op_data in op_graph.nodes.items():
        #     # TODO: should we consider tensor-wise? not operator wise?
        #     out_edges = list(op_graph.out_edges(op_id))
        #     # print(out_edges)
        #     # print()
        #     # # d['communicate_cost'] = self.comm_cost_fn(sum(parent_node['output_memory']))
        #     # # # 基因上两个算子的计算代价之和
        #     # # d['compute_cost'] = parent_node['weight'] + op_graph_ops_dict[succ_id]['weight']
        #     # print(op_data)
        #     # print(op_data['output_memory'])
        #     # print(op_data['weight'])
        #
        #     for out_edge in out_edges:
        #         next_op_id = out_edge[1]
        #         # print(str(next_op_id))
        #
        #         # pass control edges because this does not have data transfer
        #         edge_data = op_graph.get_edge_data(op_id, next_op_id)
        #         # # 跳过那些没有数据传输的节点
        #         # if ignore_control_edges and edge_data["is_control"]:
        #         #     continue
        #         comm_data = edge_data['tensor'][0]['num_bytes']
        #
        #         next_op = op_graph_ops_dict[next_op_id]
        #         compute_cost = next_op['weight']
        #         comm_cost = comm_cost_fn(comm_data)
        #
        #         # print(compute_cost)
        #         # print(comm_cost)
        #         # exit(1)
        #         # print(next_op)
        #         # print(op_data)
        #         # print(edge_data)
        #         # exit(1)
        #
        #         # 说明分开放的收益小于通信带来的开销 -> 不应该分开放
        #         if compute_cost > comm_cost:
        #             continue
        #         else:
        #             next_op['colocation_group'] = op_data['colocation_group']
        colocation_candidates = utils.ColocationGroupMap()


        for op_id, op_data in op_graph.nodes.items():
            # TODO: should we consider tensor-wise? not operator wise?
            out_edges = list(op_graph.out_edges(op_id))
            # 只考虑出度为1的节点
            if len(out_edges) != 1:
                continue

            next_op_id = out_edges[0][1]

            # pass control edges because this does not have data transfer
            edge_data = op_graph.get_edge_data(op_id, next_op_id)
            # 跳过那些没有数据传输的节点
            if ignore_control_edges and edge_data["is_control"]:
                continue

            next_op_data = op_graph.nodes[next_op_id]

            op_group = op_data['colocation_group']
            next_op_group = next_op_data['colocation_group']

            if op_group != next_op_group:
                # these two can be colocated
                _LOGGER.debug('Possible colocation ops. %s[%s] -> %s[%s]',
                              op_data['name'],
                              op_group,
                              next_op_data['name'],
                              next_op_group)
                colocation_candidates.colocate(op_group, next_op_group)


        d = {}
        for op_id, op_data in op_graph.nodes.items():
            group = op_data['colocation_group']
            if not d.__contains__(group):
                d[group] = 1
            else:
                d[group] += 1

        for k in d.keys():
            print(str(k) + " : " + str(d[k]))
        print(len(d))

        exit(1)
        return False

    def __call__(self, op_graph):
        process_colocation_group(op_graph)
        # first use default colocation group information
        if self._log_colocation_group:
            with open('tf_colocation_groups.log', 'w') as f:
                utils.print_colocation_group(
                    op_graph, print_cb=lambda v: f.write(v + '\n'))
        while self._run_colocation_step(op_graph, self._ignore_control_edges):
            pass
        if self._log_colocation_group:
            with open('coplaced_groups.log', 'w') as f:
                utils.print_colocation_group(
                    op_graph, print_cb=lambda v: f.write(v + '\n'))
        return op_graph



_GROUPER_CLASS_MAP = {
    'none': Grouper,
    'tf': TFColocationGrouper,
    'coplace': CoplacementGrouper,
    # 随机分组
    'randomplace': RandomplacementGrouper,
    # 代价分组
    'costplace': CostplacementGrouper,
}


def get_grouper(grouper=None):
    """Generates and returns a grouper instance."""
    # grouper: 1.none 2.tf 3.coplace
    grouper = grouper or FLAGS.grouper
    _LOGGER.info('Grouper: %s', grouper)
    grouper_class = _GROUPER_CLASS_MAP[grouper]
    return grouper_class()
