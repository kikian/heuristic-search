import copy

# 假设一：算子的放置只影响临近节点的性能
class GeneticGraph:
    def __init__(self, graph, id, survive_genes=None ,device_num=None):
        self.graph = graph
        self.run_time = None
        self.outputMemory = None
        self.graph_id = id
        self.device_num = device_num
        self.is_OOM = False

        # 好基因
        if survive_genes != None:
            self.survive_genes = copy.deepcopy(survive_genes)
        else:
            self.survive_genes = {}

        self.device_memory = []
        self.balence_factor = 0


